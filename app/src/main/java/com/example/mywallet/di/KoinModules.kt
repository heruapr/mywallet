package com.example.mywallet.di

import com.example.mywallet.ui.adapter.ListBankAdapter
import com.example.mywallet.ui.adapter.ListCityAdapter
import com.example.mywallet.ui.adapter.ListProvinceAdapter
import com.example.mywallet.ui.adapter.ListYearAdapter
import com.example.mywallet.db.BankDatabase
import com.example.mywallet.db.ProvinceDatabase
import com.example.mywallet.db.YearDatabase
import com.example.mywallet.repository.*
import com.example.mywallet.ui.adapter.PromoBannerAdapter
import com.example.mywallet.viewmodel.*
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.ui.adapter.HistoryAdapter
import com.example.mywallet.viewmodel.AuthViewModel
import com.example.mywallet.viewmodel.HistoryViewModel
import com.example.mywallet.viewmodel.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val dbModule = module {
    single { ProvinceDatabase.getInstance(context = get()) }
    single { YearDatabase.getInstance(context = get()) }
    single { BankDatabase.getInstance(context = get()) }
    factory { get<ProvinceDatabase>().provinceDao() }
    factory { get<YearDatabase>().yearDao() }
    factory { get<BankDatabase>().bankDao() }
}

val repositoryModule = module {
    single {
        MyWalletRepository()
    }
    single {
        ProvinceRepository(get())
    }
    single {
        YearRepository(get())
    }
    single {
        BankRepository(get())
    }
}

val uiModule = module {
    factory { PromoBannerAdapter() }
    factory { ListProvinceAdapter() }
    factory { ListCityAdapter() }
    factory { ListYearAdapter() }
    factory { ListBankAdapter() }
    factory { HistoryAdapter() }
    viewModel { AuthViewModel(get()) }
    viewModel { TransactionViewModel(get()) }
    viewModel { ProvinceViewModel(get()) }
    viewModel { CityViewModel(get()) }
    viewModel { YearViewModel(get()) }
    viewModel { BankViewModel(get()) }
    viewModel { MainViewModel(get()) }
    viewModel { HistoryViewModel(get()) }
}