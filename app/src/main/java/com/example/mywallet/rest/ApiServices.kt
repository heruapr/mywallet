package com.example.mywallet.rest

import com.example.mywallet.model.*
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ApiServices {

    @POST("register")
    fun register(@Body registerRequest: RegisterRequest): Call<BaseResponse<User>>

    @POST("login")
    fun login(@Body loginRequest: LoginRequest): Call<BaseResponse<User>>

    @POST("register/otp-validation")

    fun otpValidation(@Body otpValidationRequest: OTPValidationRequest): Call<BaseResponse<String>>

    @PUT("user/{id_user}/update-pin")
    fun updatePin(@Body updatePINRequest: UpdatePINRequest, @Path("id_user") idUser: Int, @Query("token") token: String): Call<BaseResponse<String>>

    @POST("register/request-activation")
    fun resendOtp(@Body resendOTPRequest: ResendOTPRequest): Call<BaseResponse<User>>

    @POST("user/{id_user}/otp-new-telephone")
    fun otpValidationNewNumber(
        @Body otpValidationRequest: OTPValidationRequest, @Path("id_user") idUser: Int,
        @Query("token") token: String
    ): Call<BaseResponse<String>>

    @POST("user/{id_user}/logout")
    fun logout(@Path("id_user") userId: Int, @Query("token") token: String): Call<BaseResponse<Any>>

    @GET("user/{id}")
    fun getUserData(@Path("id") userId: Int, @Query("token") token: String): Call<BaseResponse<User>>

    @GET("user/{id}/transaction")
    fun getAllTransaction(@Path("id") userId: Int, @Query("token") token: String): Call<BaseResponse<Transactions>>

    @PUT("user/{id}")
    fun updateUserData(
        @Body updateUserDataRequest: UpdateUserDataRequest, @Path("id") userId: Int, @Query(
            "token"
        ) token: String
    ): Call<BaseResponse<User>>

    @POST("user/{id_user}/match-pin")
    fun checkPin(@Body checkPinRequest: CheckPinRequest, @Path("id_user") idUser: Int, @Query("token") token: String): Call<BaseResponse<Any>>

    //TRANSACTION METHOD
    @GET("province")
    fun getProvince(): Call<JsonObject>

    @GET("year")
    fun getYear(): Call<JsonObject>

    @GET("bank")
    fun getBank(): Call<JsonObject>

    @GET("province/{id_province}/city")
    fun getCity(@Path("id_province") path: Int): Call<BaseResponse<CityList>>

    @POST("user/{id}/tax-payment")
    fun getBill(@Body checkBillRequest: CheckBillRequest, @Path("id") idUser: Int, @Query("token") token: String): Call<BaseResponse<Bill>>

    @POST("user/{id}/transaction-by-ewallet")
    fun getEwalletPay(
        @Body ewalletPaymentRequest: eWalletPaymentRequest, @Path("id") idUser: Int, @Query(
            "token"
        ) token: String
    ): Call<BaseResponse<Any>>

    @POST("user/{id}/transaction-by-virtual-account")
    fun getBankPay(
        @Body bankPaymentRequest: BankPaymentRequest, @Path("id") idUser: Int, @Query(
            "token"
        ) token: String
    ): Call<BaseResponse<BankPayment>>


    @GET("user/{id}/transaction/{id_transaction}")
    fun getSingleTransaction(
        @Path("id") userId: Int, @Path("id_transaction") transactionId: Int, @Query(
            "token"
        ) token: kotlin.String
    ): Call<BaseResponse<Transaction>>


    @Multipart
    @PUT("user/{id_user}/upload")
    fun uploadPhoto(
        @Part photo: MultipartBody.Part, @Path("id_user") idUser: Int, @Query(
            "token"
        ) token: String
    ): Call<BaseResponse<User>>
}