package com.example.mywallet.rest

import android.app.Application
import com.example.mywallet.di.dbModule
import com.example.mywallet.di.repositoryModule
import com.example.mywallet.di.uiModule
import com.example.mywallet.util.Constants.BASE_URL
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ApiClient : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            //inject Android context
            androidContext(this@ApiClient)
            // use modules
            modules(listOf(
                dbModule,
                repositoryModule,
                uiModule
            ))
        }
    }
    private val client = OkHttpClient().newBuilder()
        .addInterceptor { chain ->
            val request = chain.request()
                .newBuilder()
                .build()
            chain.proceed(request)
        }

    var gson = GsonBuilder()
        .setLenient()
        .create()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL) //base URL disimpan di object Constant.kt
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    val services: ApiServices = retrofit.create(ApiServices::class.java)
}