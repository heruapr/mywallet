package com.example.mywallet.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.mywallet.db.dao.ProvinceDao
import com.example.mywallet.db.entity.Province

class ProvinceRepository(private val provinceDao: ProvinceDao) {

    private val allProvinces: LiveData<List<Province>> = provinceDao.getAllProvinces()

    //FUN DB LIST PROVINSI FOR DAO
    fun insert(province: Province) {
        InsertProvinceAsyncTask(
            provinceDao
        ).execute(province)
    }

    fun deleteAllProvinces() {
        DeleteAllProvincesAsyncTask(
            provinceDao
        ).execute()
    }

    fun getAllProvinces(): LiveData<List<Province>> {
        return allProvinces
    }

    //PROVINSI

    private class InsertProvinceAsyncTask(val provinceDao: ProvinceDao) : AsyncTask<Province, Unit, Unit>() {
        override fun doInBackground(vararg province: Province?) {
            provinceDao.insert(province[0]!!)
        }
    }

    private class DeleteAllProvincesAsyncTask(val provinceDao: ProvinceDao) : AsyncTask<Unit, Unit, Unit>() {
        override fun doInBackground(vararg p0: Unit?) {
            provinceDao.deleteAllProvinces()
        }
    }
}