package com.example.mywallet.repository

import android.util.Log
import com.example.mywallet.model.*
import com.example.mywallet.rest.ApiClient
import com.example.mywallet.util.Constants.API_INVALID_OTP_MESSAGE
import com.example.mywallet.util.Constants.API_LOGOUT_SUCCESS_MESSAGE
import com.example.mywallet.util.Constants.API_RESEND_OTP_SUCCESS_MESSAGE
import com.example.mywallet.util.Constants.TAG
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyWalletRepository {

    fun register(registerRequest: RegisterRequest, callback: RegisterCallback) {

        ApiClient().services.register(registerRequest)
            .enqueue(object : Callback<BaseResponse<User>> {
                override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error login : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<User>>,
                    response: Response<BaseResponse<User>>
                ) {
                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 201) {
                            Log.d(TAG, "DATA = ${body.data}")
                            Log.d(TAG, "DATA = ${body.data.idUser}")
                            body.data.let { it ->
                                //                            Log.d(TAG, "data : ${it.idUser}")

                                callback.onSuccess(it, response.body()?.message.toString())
                            }
                            Log.d(TAG, "register success : $body")
                        } else {

                            callback.onFailure(body!!.message)
                            Log.d(TAG, "register error : $body")
                        }
                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error register : ${response.body()}")
                    }

                }
            })
    }

    fun login(loginRequest: LoginRequest, callback: LoginCallback) {

        ApiClient().services.login(loginRequest).enqueue(object : Callback<BaseResponse<User>> {
            override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                callback.onFailure(t.localizedMessage)
                Log.d(TAG, "error login : ${t.localizedMessage}")
            }

            override fun onResponse(
                call: Call<BaseResponse<User>>,
                response: Response<BaseResponse<User>>
            ) {

                if (response.isSuccessful) {
                    val body = response.body()
                    if (body?.status == 200) {
                        body.data.let { it ->
                            Log.d(TAG, "data : ${it.idUser}")

                            callback.onSuccess(it, response.body()?.message.toString())
                        }
                        Log.d(TAG, "login success : $body")
                    } else {
                        callback.onFailure(body!!.message)
                        Log.d(TAG, "login error : $body")
                    }

                } else {
                    callback.onFailure(response.message())
                    Log.d(TAG, "error login : ${response.body()}")

                }

            }
        })
    }

    fun otpValidation(otpValidationRequest: OTPValidationRequest, callback: OTPValidationCallback) {

        ApiClient().services.otpValidation(otpValidationRequest)
            .enqueue(object : Callback<BaseResponse<String>> {
                override fun onFailure(call: Call<BaseResponse<String>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error otp validation : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<String>>,
                    response: Response<BaseResponse<String>>
                ) {

                    Log.d(
                        TAG,
                        "response = $response \nOTP = ${otpValidationRequest.otp}\nID = ${otpValidationRequest.idUser}"
                    )
                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            if (body.message.equals(API_INVALID_OTP_MESSAGE, true)) {
                                callback.onFailure(body.message)
                            } else {
                                callback.onSuccess(body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "otp validation error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error otp validation : ${response.body()}")

                    }

                }
            })
    }

    fun otpValidationNewNumber(
        otpValidationRequest: OTPValidationRequest,
        idUser: Int,
        token: String,
        callback: OTPValidationCallback
    ) {

        Log.d(
            TAG,
            "ID = ${otpValidationRequest.idUser} \n OTP = ${otpValidationRequest.otp}\n token = $token"
        )

        ApiClient().services.otpValidationNewNumber(
            otpValidationRequest,
            otpValidationRequest.idUser,
            token.trim()
        )
            .enqueue(object : Callback<BaseResponse<String>> {
                override fun onFailure(call: Call<BaseResponse<String>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error otp validation new: ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<String>>,
                    response: Response<BaseResponse<String>>
                ) {

                    Log.d(
                        TAG,
                        "response = $response \nOTP = ${otpValidationRequest.otp}\nID = ${idUser}\n Token = ${token.trim()}"
                    )
                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            if (body.message.equals(API_INVALID_OTP_MESSAGE, true)) {
                                callback.onFailure(body.message)
                            } else {
                                callback.onSuccess(body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "otp validation new error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error otp new validation : ${response.errorBody()?.string()}")

                    }

                }
            })
    }


    fun updatePin(
        updatePINRequest: UpdatePINRequest,
        idUser: Int,
        token: String,
        callback: UpdatePINCallback
    ) {

        ApiClient().services.updatePin(updatePINRequest, idUser, token)
            .enqueue(object : Callback<BaseResponse<String>> {
                override fun onFailure(call: Call<BaseResponse<String>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error update PIN : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<String>>,
                    response: Response<BaseResponse<String>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            callback.onSuccess(body.message)
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "update PIN error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error update PIN : ${response.body()}")

                    }

                }
            })
    }


    fun resendOtp(resendOTPRequest: ResendOTPRequest, callback: ResendOTPCallback) {

        ApiClient().services.resendOtp(resendOTPRequest)
            .enqueue(object : Callback<BaseResponse<User>> {
                override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error resend OTP : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<User>>,
                    response: Response<BaseResponse<User>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            if (body.message.equals(API_RESEND_OTP_SUCCESS_MESSAGE, true)) {
                                val user = body.data
                                val idUser = user.idUser
                                callback.onSuccess(idUser, body.message)
                            } else {
                                callback.onFailure(body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "resend OTP error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error resend OTP : ${response.errorBody()}")

                    }

                }
            })
    }


    fun logout(idUser: Int, token: String, callback: LogoutCallback) {

        ApiClient().services.logout(idUser, token)
            .enqueue(object : Callback<BaseResponse<Any>> {
                override fun onFailure(call: Call<BaseResponse<Any>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error failure logout : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<Any>>,
                    response: Response<BaseResponse<Any>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            if (body.message.equals(API_LOGOUT_SUCCESS_MESSAGE, true)) {
                                callback.onSuccess(body.message)
                            } else {
                                callback.onFailure(body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "logout error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error logout : ${response.body()}")

                    }

                }
            })
    }

    fun getUserData(userId: Int, token: String, callback: GetUserDataCallback) {

        ApiClient().services.getUserData(userId, token)
            .enqueue(object : Callback<BaseResponse<User>> {
                override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error get user data : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<User>>,
                    response: Response<BaseResponse<User>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            body.data.let {
                                callback.onSuccess(it, body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "get user data error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error get user data : ${response.body()}")

                    }

                }
            })
    }

    fun getAllTransaction(userId: Int, token: String, callback: GetAllTransactionCallback) {

        ApiClient().services.getAllTransaction(userId, token)
            .enqueue(object : Callback<BaseResponse<Transactions>> {
                override fun onFailure(call: Call<BaseResponse<Transactions>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error failure get all trx : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<Transactions>>,
                    response: Response<BaseResponse<Transactions>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            body.data.let {
                                Log.d(TAG, "transactions = ${it.transactions}")
                                callback.onSuccess(it.transactions, body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "get all trx error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error get all trx : ${response.body()}")

                    }

                }
            })
    }

    fun updateUserData(
        updateUserDataRequest: UpdateUserDataRequest,
        idUser: Int,
        token: String,
        callback: UpdateProfileCallback
    ) {

        ApiClient().services.updateUserData(
            updateUserDataRequest,
            idUser,
            token
        )
            .enqueue(object : Callback<BaseResponse<User>> {
                override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error failure update profile : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<User>>,
                    response: Response<BaseResponse<User>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            body.data.let {
                                callback.onSuccess(it, body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "update profile not success : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error update profile : ${response.errorBody()}")
                        Log.d(TAG, "error update profile: $response")

                    }

                }
            })
    }

    fun uploadPhoto(
        filePart: MultipartBody.Part,
        idUser: Int,
        token: String,
        callback: UploadPhotoCallback
    ) {

        ApiClient().services.uploadPhoto(
            filePart,
            idUser,
            token
        ).enqueue(object : Callback<BaseResponse<User>> {
            override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                callback.onFailure(t.localizedMessage)
                Log.d(TAG, "error failure update profile photo: ${t.localizedMessage}")
            }

            override fun onResponse(
                call: Call<BaseResponse<User>>,
                response: Response<BaseResponse<User>>
            ) {

                if (response.isSuccessful) {
                    val body = response.body()
                    if (body?.status == 200) {
                        body.data.let {
                            callback.onSuccess(it, body.message)
                        }
                    } else {
                        callback.onFailure(body!!.message)
                        Log.d(TAG, "update profile photo not success : $body")
                    }

                } else {
                    callback.onFailure(response.message())
                    Log.d(TAG, "error update profile photo: ${response.errorBody()?.string()}")

                }

            }
        })
    }

    fun checkPin(
        checkPinRequest: CheckPinRequest,
        userId: Int,
        token: String,
        callback: CheckPinCallback
    ) {

        ApiClient().services.checkPin(checkPinRequest, userId, token)
            .enqueue(object : Callback<BaseResponse<Any>> {
                override fun onFailure(call: Call<BaseResponse<Any>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error failure check pin : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<Any>>,
                    response: Response<BaseResponse<Any>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            callback.onSuccess(body.message)
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "check pin error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error check pin : ${response.body()}")
                        Log.d(TAG, "error check pin : $response")
                    }

                }
            })
    }

    fun getListCity(idProvince: Int, callback: GetListCityCallback) {

        ApiClient().services.getCity(idProvince)
            .enqueue(object : Callback<BaseResponse<CityList>> {
                override fun onFailure(call: Call<BaseResponse<CityList>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error failure get city : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<CityList>>,
                    response: Response<BaseResponse<CityList>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            body.data.let {
                                Log.d(TAG, "provinces = ${it.cityList}")
                                callback.onSuccess(it.cityList, body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "get all error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error get all: ${response.body()}")

                    }

                }
            })
    }

    //e wallet pay
    fun getWalletPayment(
        userId: Int,
        token: String,
        ewalletPaymentRequest: eWalletPaymentRequest,
        callback: getWalletPaymentCallback
    ) {

        val params: HashMap<String, String> = HashMap()
        //params["token"] = "e3818459-45aa-4757-b8a9-c4bd6f94756f"

        ApiClient().services.getEwalletPay(ewalletPaymentRequest, userId, token)
            .enqueue(object : Callback<BaseResponse<Any>> {
                override fun onFailure(call: Call<BaseResponse<Any>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error getWalletPayment 1 : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<Any>>,
                    response: Response<BaseResponse<Any>>
                ) {
                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 201) {
                            //body.data.let { it ->
//                                Log.d(
//                                    TAG, "data : ${it.get("message")}"
//                                )
                            callback.onSuccess(body.message.toString())
                            //}
                            Log.d(TAG, "MANTAP : $body")
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "getting wallet payment error : $body")
                        }
                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "not connected to API endPoint")

                    }

                }
            })
    }

    //virtual account pay
    fun getBankPayment(
        userId: Int,
        token: String,
        bankPaymentRequest: BankPaymentRequest,
        callback: getBankPaymentCallback
    ) {
        val params: HashMap<String, String> = HashMap()
        //params["token"] = "e3818459-45aa-4757-b8a9-c4bd6f94756f"
        ApiClient().services.getBankPay(bankPaymentRequest, userId, token)
            .enqueue(object : Callback<BaseResponse<BankPayment>> {
                override fun onFailure(call: Call<BaseResponse<BankPayment>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error Bank payment 1 : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<BankPayment>>,
                    response: Response<BaseResponse<BankPayment>>
                ) {
                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 201) {

                            body.data.let { it ->
                                Log.d(
                                    TAG, "data : ${it.virtualNumber}"
                                )

                                callback.onSuccess(it, body.message.toString())
                            }
                            Log.d(TAG, "MANTAP : $body")
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "getting bank payment error : $body")
                        }
                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "not connected to API endPoint")

                    }

                }
            })
    }

    fun getBill(
        userId: Int,
        token: String,
        checkBillRequest: CheckBillRequest,
        callback: getBillCallback
    ) {
        ApiClient().services.getBill(checkBillRequest, userId, token)
            .enqueue(object : Callback<BaseResponse<Bill>> {
                override fun onFailure(call: Call<BaseResponse<Bill>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error getBill 1 : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<Bill>>,
                    response: Response<BaseResponse<Bill>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status.toString() == "200") {

                            body?.data.let { it ->
                                Log.d(
                                    TAG, "data : ${it!!.taxNumber}"
                                )

                                callback.onSuccess(it, response.body()?.message.toString())
                            }
                            Log.d(TAG, "MANTAP : $body")
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "getting bill error : $body")
                        }

                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "getting bill error 3 : ${response.body()}")
                    }
                }
            })
    }

    fun getSingleTransaction(
        userId: Int,
        transactionId: Int,
        token: String,
        callback: GetSingleTransactionCallback
    ) {

        ApiClient().services.getSingleTransaction(userId, transactionId, token)
            .enqueue(object : Callback<BaseResponse<Transaction>> {
                override fun onFailure(call: Call<BaseResponse<Transaction>>, t: Throwable) {
                    callback.onFailure(t.localizedMessage)
                    Log.d(TAG, "error failure get all trx : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<Transaction>>,
                    response: Response<BaseResponse<Transaction>>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body?.status == 200) {
                            body.data.let {
                                Log.d(TAG, "transactions = ${it.isFinished}")
                                callback.onSuccess(it!!, body.message)
                            }
                        } else {
                            callback.onFailure(body!!.message)
                            Log.d(TAG, "get trx error : $body")
                        }
                    } else {
                        callback.onFailure(response.message())
                        Log.d(TAG, "error get trx : ${response.body()}")

                    }

                }
            })
    }

    interface RegisterCallback {
        fun onSuccess(user: User?, message: String)
        fun onFailure(message: String)
    }


    interface LoginCallback {
        fun onSuccess(user: User?, message: String)
        fun onFailure(message: String)
    }

    interface OTPValidationCallback {
        fun onSuccess(message: String)
        fun onFailure(message: String)
    }

    interface ResendOTPCallback {
        fun onSuccess(idUser: Int, message: String)
        fun onFailure(message: String)
    }

    interface UpdatePINCallback {
        fun onSuccess(message: String)
        fun onFailure(message: String)
    }

    interface LogoutCallback {
        fun onSuccess(message: String)
        fun onFailure(message: String)
    }

    interface GetListCityCallback {
        fun onSuccess(cityList: List<City>, message: String)
        fun onFailure(message: String)
    }

    interface GetUserDataCallback {
        fun onSuccess(user: User?, message: String)
        fun onFailure(message: String)
    }

    interface GetAllTransactionCallback {
        fun onSuccess(transactionList: List<Transaction>?, message: String)
        fun onFailure(message: String)
    }

    interface UpdateProfileCallback {
        fun onSuccess(user: User?, message: String)
        fun onFailure(message: String)
    }

    interface UploadPhotoCallback {
        fun onSuccess(user: User?, message: String)
        fun onFailure(message: String)
    }

    interface CheckPinCallback {
        fun onSuccess(message: String)
        fun onFailure(message: String)
    }

    interface getBillCallback {
        fun onSuccess(bill: Bill, message: String)
        fun onFailure(message: String)
    }

    interface getWalletPaymentCallback {
        fun onSuccess(message: String)
        fun onFailure(message: String)
    }

    interface getBankPaymentCallback {
        fun onSuccess(bankPayment: BankPayment, message: String)
        fun onFailure(message: String)
    }

    interface GetSingleTransactionCallback {
        fun onSuccess(transaction: Transaction, message: String)
        fun onFailure(message: String)
    }
}

