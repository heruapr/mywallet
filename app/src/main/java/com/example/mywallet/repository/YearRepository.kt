package com.example.mywallet.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.mywallet.db.dao.YearDao
import com.example.mywallet.db.entity.Year

class YearRepository(private val yearDao: YearDao) {

    private val allYears: LiveData<List<Year>> = yearDao.getAllYears()

    //FUN DB LIST PROVINSI FOR DAO
    fun insert(year: Year) {
        InsertYearAsyncTask(
            yearDao
        ).execute(year)
    }

    fun deleteAllYears() {
        DeleteAllYearsAsyncTask(
            yearDao
        ).execute()
    }

    fun getAllYears(): LiveData<List<Year>> {
        return allYears
    }

    //PROVINSI

    private class InsertYearAsyncTask(val yearDao: YearDao) : AsyncTask<Year, Unit, Unit>() {
        override fun doInBackground(vararg year: Year?) {
            yearDao.insert(year[0]!!)
        }
    }

    private class DeleteAllYearsAsyncTask(val yearDao: YearDao) : AsyncTask<Unit, Unit, Unit>() {
        override fun doInBackground(vararg p0: Unit?) {
            yearDao.deleteAllYears()
        }
    }
}