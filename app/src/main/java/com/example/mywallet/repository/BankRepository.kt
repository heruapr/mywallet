package com.example.mywallet.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.mywallet.db.dao.BankDao
import com.example.mywallet.db.entity.Bank

class BankRepository(private val bankDao: BankDao) {

    private val allBanks: LiveData<List<Bank>> = bankDao.getAllBanks()

    //FUN DB LIST PROVINSI FOR DAO
    fun insert(bank: Bank) {
        InsertBankAsyncTask(
            bankDao
        ).execute(bank)
    }

    fun deleteAllBanks() {
        DeleteAllBanksAsyncTask(
            bankDao
        ).execute()
    }

    fun getAllBanks(): LiveData<List<Bank>> {
        return allBanks
    }

    //PROVINSI

    private class InsertBankAsyncTask(val bankDao: BankDao) : AsyncTask<Bank, Unit, Unit>() {
        override fun doInBackground(vararg bank: Bank?) {
            bankDao.insert(bank[0]!!)
        }
    }

    private class DeleteAllBanksAsyncTask(val bankDao: BankDao) : AsyncTask<Unit, Unit, Unit>() {
        override fun doInBackground(vararg p0: Unit?) {
            bankDao.deleteAllBanks()
        }
    }
}