package com.example.mywallet.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.example.mywallet.ui.activity.PBBActivity
import com.example.mywallet.R

class ListProvinceAdapter : RecyclerView.Adapter<ListProvinceAdapter.ListProvinceHolder>() {
    private var listProvince: List<com.example.mywallet.db.entity.Province> = ArrayList()
    //    var listProvince = MutableLiveData<MutableList<Province>>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListProvinceHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_province, parent, false)
        return ListProvinceHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listProvince.size
    }

    override fun onBindViewHolder(holder: ListProvinceHolder, position: Int) {
        val currentProvince = listProvince[position]
        holder.textViewProvince.text = currentProvince.provinceName
        holder.textViewProvince.setOnClickListener {
            val activity = it.getContext() as PBBActivity
            val ft: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
            ft.setCustomAnimations(R.animator.anim_slide_up, 0, 0, R.animator.anim_slide_down)
            activity.onBackPressed()
            val preferences = activity.getSharedPreferences("PreferenceApp", 0)
            val editor = preferences.edit()
            editor.putString("selected_province", currentProvince.provinceName)
            editor.putInt("current_id_province", currentProvince.idProvince)
            editor.apply()
            activity.readFieldData()
            ft.commit()
        }
    }

    inner class ListProvinceHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewProvince: TextView = itemView.findViewById(R.id.text_view_item_province)
    }

    fun setProvince(listProvince: List<com.example.mywallet.db.entity.Province>) {
        this.listProvince = listProvince
        notifyDataSetChanged()
    }

}