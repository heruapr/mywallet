package com.example.mywallet.ui.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.mywallet.R
import com.example.mywallet.util.Constants
import com.example.mywallet.util.Constants.NAME_REGEX
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_register.*

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun setupUI() {
        edit_text_name_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_name_register, text_input_layout_name, "Nama")) {
                    clearError(text_input_layout_name)
                    showValidSign(text_input_layout_name)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        edit_text_email_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_email_register, text_input_layout_email, "Email")) {
                    clearError(text_input_layout_email)
                    showValidSign(text_input_layout_email)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        edit_text_phone_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(
                        edit_text_phone_register,
                        text_input_layout_phone,
                        "Nomor telepon"
                    )
                ) {
                    clearError(text_input_layout_phone)
                    showValidSign(text_input_layout_phone)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        edit_text_password_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(
                        edit_text_password_register,
                        text_input_layout_password,
                        "Password"
                    )
                ) {
                    clearError(text_input_layout_password)
                    showValidSign(text_input_layout_password)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        edit_text_password_confirmation_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_password_confirmation_register, text_input_layout_password_confirmation,
                        "Konfirmasi password")){
                    clearError(text_input_layout_password_confirmation)
                    showValidSign(text_input_layout_password_confirmation)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun isFieldValid(
        editText: TextInputEditText,
        textInputLayout: TextInputLayout,
        name: String
    ): Boolean {
        if (editText.text.toString().isNotEmpty()) {
            when (editText) {
                edit_text_name_register -> {
                    if (!editText.text.toString().matches(NAME_REGEX)){
                        setFieldError(textInputLayout, getString(R.string.error_name_not_valid))
                        return false
                    }
                }
                edit_text_email_register -> {
                    if (!editText.text.toString().matches(Constants.EMAIL_REGEX)) {
                        setFieldError(textInputLayout, getString(R.string.error_email_not_valid))
                        return false
                    }
                }

                edit_text_password_register -> {
                    if (!editText.text.toString().matches(Constants.PASSWORD_REGEX)) {
                        setFieldError(textInputLayout, getString(R.string.error_password_not_valid))
                        return false
                    }
                }

                edit_text_password_confirmation_register -> {
                    if (!editText.text.toString().matches(Constants.PASSWORD_REGEX)) {
                        setFieldError(textInputLayout, getString(R.string.error_password_not_valid))
                        return false
                    }

                    if (editText.text.toString() != edit_text_password_register.text.toString()){
                        setFieldError(textInputLayout, getString(R.string.error_confirm_password_not_match))
                        return false
                    }
                }

                edit_text_phone_register -> {
                    if (!editText.text.toString().matches(Constants.PHONE_REGEX)) {
                        setFieldError(textInputLayout, getString(R.string.error_phone_not_valid))
                        return false
                    }
                }
            }
        } else {
            setFieldError(textInputLayout, getString(R.string.error_field_cannot_empty, name))
            return false
        }

        return true

    }

    private fun setFieldError(textInputLayout: TextInputLayout, message: String) {
        textInputLayout.error = message
    }

    private fun clearError(textInputLayout: TextInputLayout) {
        textInputLayout.error = null
    }

    private fun showValidSign(textInputLayout: TextInputLayout) {
        textInputLayout.boxStrokeColor = resources.getColor(R.color.colorGreenSuccess)
    }

    fun isAllFieldValid(): Boolean {
        isFieldValid(edit_text_name_register, text_input_layout_name, "Nama")
        isFieldValid(edit_text_email_register, text_input_layout_email, "Email")
        isFieldValid(edit_text_phone_register, text_input_layout_phone, "Nomor telepon")
        isFieldValid(edit_text_password_register, text_input_layout_password, "Password")
        isFieldValid(edit_text_password_confirmation_register, text_input_layout_password_confirmation, "Konfirmasi password")

        return (text_input_layout_name.error.isNullOrEmpty() && text_input_layout_email.error.isNullOrEmpty()
                && text_input_layout_phone.error.isNullOrEmpty() && text_input_layout_password.error.isNullOrEmpty()
                && text_input_layout_password_confirmation.error.isNullOrEmpty())
    }

}
