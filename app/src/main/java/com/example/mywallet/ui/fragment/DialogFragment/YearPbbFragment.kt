package com.example.mywallet.ui.fragment.DialogFragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.mywallet.R
import com.example.mywallet.ui.adapter.ListYearAdapter
import com.example.mywallet.db.entity.Year
import com.example.mywallet.rest.ApiClient
import com.example.mywallet.util.Constants
import com.example.mywallet.viewmodel.YearViewModel
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_year_pbb.*
import org.json.JSONObject
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class YearPbbFragment : Fragment() {
    private val adapter: ListYearAdapter by inject()
    private val yearViewModel: YearViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_year_pbb, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        year_recyclerview.layoutManager = LinearLayoutManager(this.requireContext())
        year_recyclerview.setHasFixedSize(true)
        year_recyclerview.adapter = adapter
        setupRecyclerView()
        getYear()

        button_close.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun setupRecyclerView() {
        showProgressBar(false)
        yearViewModel.getAllYears().observe(this,
            Observer<List<Year>> { list ->
                list?.let {
                    adapter.setYear(it)
                }
            })
        Log.d("SAMPAI OBSERVER", "sampai OBSERVER")
    }

    fun getYear() {
        ApiClient().services.getYear()
            .enqueue(object : Callback<JsonObject> {
                override fun onFailure(
                    call: Call<JsonObject>,
                    t: Throwable
                ) {
                    Log.d(Constants.TAG, "error getYear 1 : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if(JSONObject(body.toString()).getInt("status").equals(200)) {
                            writeData(body.toString())
                        }else{
                            Log.d(Constants.TAG,"error server")
                        }
                        Log.d(Constants.TAG, "MANTAP year : $body")
                    } else {
                        Log.d(Constants.TAG, "getting year error :")
                    }
                }
            })
    }

    fun writeData(response: String) {
        showProgressBar(true)
        val obj = JSONObject(response).getJSONObject("data")
        val dataArray = obj.getJSONArray("years")
        Log.d("WRITE DATA", "sampai WRITE , lengt array obj: ${dataArray.length()}")
        for (i in 0..dataArray.length() - 1) {
            val dataobj = dataArray.getInt(i)
            //save to sqlite
            yearViewModel.insert(
                Year(
                    dataobj
                )
            )
            Log.d("SIMPAANN", "SAVED DATA")
        }
        setupRecyclerView()
    }
    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            progress_bar.visibility = View.VISIBLE
        } else {
            progress_bar.visibility = View.GONE
        }
    }
}

