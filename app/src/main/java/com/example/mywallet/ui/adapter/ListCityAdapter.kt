package com.example.mywallet.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.example.mywallet.ui.fragment.DialogFragment.ListCityFragment
import com.example.mywallet.ui.activity.PBBActivity
import com.example.mywallet.R

class ListCityAdapter() : RecyclerView.Adapter<ListCityAdapter.ListCityHolder>() {
    private var listCity: List<com.example.mywallet.model.City> = ArrayList()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListCityHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_city, parent, false)
        return ListCityHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listCity.size
    }


    override fun onBindViewHolder(holder: ListCityHolder, position: Int) {
        val currentCity = listCity[position]
        holder.textViewCity.text = currentCity.cityName
        holder.textViewCity.setOnClickListener {
            val activity = it.getContext() as PBBActivity
            val dialogFragment = ListCityFragment()
            val ft: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
            ft.setCustomAnimations(R.animator.anim_slide_up, 0, 0, R.animator.anim_slide_down)
            //ft.replace(R.id.frameLayout2, dialogFragment)
            activity.onBackPressed()
            val preferences = activity.getSharedPreferences("PreferenceApp", 0)
            val editor = preferences.edit()
            editor.putString("selected_city", currentCity.cityName)
            editor.putInt("current_id_city", currentCity.idCity)
            editor.apply()
            activity.readFieldData()
            ft.commit()
        }
    }

    inner class ListCityHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewCity: TextView = itemView.findViewById(R.id.text_view_item_city)
    }

    fun setList(listCity: List<com.example.mywallet.model.City>) {
        this.listCity = listCity
        notifyDataSetChanged()
    }
}