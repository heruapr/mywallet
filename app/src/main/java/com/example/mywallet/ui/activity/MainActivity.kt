package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mywallet.R
import com.example.mywallet.model.User
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.ui.adapter.PromoBannerAdapter
import com.example.mywallet.util.Constants.BASE_URL
import com.example.mywallet.util.Constants.PREF_KEY_PHOTO_URL
import com.example.mywallet.util.Constants.PREF_KEY_TOKEN
import com.example.mywallet.util.Constants.PREF_KEY_USER_BALANCE
import com.example.mywallet.util.Constants.PREF_KEY_USER_ID
import com.example.mywallet.util.Constants.PREF_KEY_USER_NAME
import com.example.mywallet.util.Constants.PREF_NAME
import com.example.mywallet.util.Constants.TAG
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import java.util.*
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    companion object {
        const val USER_DATA_EXTRA = "user_data_extra"
    }

    private lateinit var bannerAdapter: PromoBannerAdapter
    private var bannerList = arrayListOf<Drawable>()
    private lateinit var sharedPreferences: SharedPreferences
    private val viewModel: MainViewModel by inject()
    private lateinit var timer: Timer
    private lateinit var autoScrollTask: AutoScrollTask


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        window.statusBarColor = Color.TRANSPARENT

        //setupRecyclerView()

        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        setupUI()

        bannerList.add(getDrawable(R.drawable.banner_1)!!)
        bannerList.add(getDrawable(R.drawable.banner_2)!!)
        bannerList.add(getDrawable(R.drawable.banner_1)!!)
        bannerList.add(getDrawable(R.drawable.banner_2)!!)
        bannerList.add(getDrawable(R.drawable.banner_1)!!)

        checkPin()
        if (intent.getParcelableExtra<User>(USER_DATA_EXTRA) != null) {
            val user = intent.getParcelableExtra(USER_DATA_EXTRA) as User
            //checkPin(user)
        }
    }

    private fun setupUI() {
        getBalance()
        setupRecyclerView()
        val name = sharedPreferences.getString(PREF_KEY_USER_NAME, "")
        val balance = sharedPreferences.getFloat(PREF_KEY_USER_BALANCE, 0f).roundToInt()
        val photo = sharedPreferences.getString(PREF_KEY_PHOTO_URL, "")

        text_view_greetings.text = "Halo, ${name?.substringBefore(" ")}!"
        card_view_menu_bayar_pbb.setOnClickListener {
            val intent = Intent(this, PBBActivity::class.java)
            startActivity(intent)
        }

        if (photo != "") {
            Glide.with(this).load("$BASE_URL$photo").into(image_view_avatar)
        }
        Log.d(TAG, "$BASE_URL$photo")
        image_view_avatar.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
        }

        startAutoScrollBanner()
    }


    private fun setupRecyclerView() {
        bannerAdapter = PromoBannerAdapter()
        bannerAdapter.setList(bannerList)

        recycler_view_banner.apply {
            adapter = bannerAdapter
            layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.HORIZONTAL, false)
            onFlingListener = null
        }
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(recycler_view_banner)
    }

    private fun checkPin() {
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")
        viewModel.getUserData(
            idUser,
            token.toString(),
            object : MyWalletRepository.GetUserDataCallback {
                override fun onSuccess(user: User?, message: String) {
                    if (user?.pin == null) {
                        val intent = Intent(this@MainActivity, PinActivity::class.java)
                        intent.putExtra(PinActivity.TYPE_EXTRA, "create")
                        startActivity(intent)
                        finish()
                    }
                    toast(this@MainActivity, message)
                }

                override fun onFailure(message: String) {
                    toast(this@MainActivity, message)
                }
            })
    }

    private fun getBalance() {
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")
        viewModel.getUserData(
            idUser,
            token.toString(),
            object : MyWalletRepository.GetUserDataCallback {
                override fun onSuccess(user: User?, message: String) {
                   val balance = user?.balance?.roundToInt()
                    text_view_balance.text = "Rp. $balance"
                    saveUserPreference(user)
                }

                override fun onFailure(message: String) {
                    toast(this@MainActivity, message)
                }
            })
    }

    private fun startAutoScrollBanner() {
        recycler_view_banner.scrollToPosition(0)
        autoScrollTask = AutoScrollTask()
        timer = Timer()
        timer.scheduleAtFixedRate(autoScrollTask, 2000, 5000)
    }

    private fun stopAutoScrollBanner() {
        timer.cancel()
        timer.purge()
        autoScrollTask.cancel()
        recycler_view_banner.scrollToPosition(0)
    }

    private fun saveUserPreference(user: User?) {
        val editor = sharedPreferences.edit()
        editor.putFloat(PREF_KEY_USER_BALANCE, user?.balance!!.toFloat())
        editor.apply()
        editor.commit()
    }

    override fun onResume() {
        super.onResume()
        setupUI()
    }

    override fun onPause() {
        super.onPause()
        stopAutoScrollBanner()
    }


    inner class AutoScrollTask : TimerTask() {
        var position = 0
        var isEnd = false
        var isStarted = false

        override fun run() {
            isStarted = true

            if (position == bannerList.lastIndex) {
                isEnd = true
            } else if (position == 0) {
                isEnd = false
            }

            if (!isEnd) {
                position++
            } else {
                position--
            }
            recycler_view_banner.smoothScrollToPosition(position)
        }

    }
}


