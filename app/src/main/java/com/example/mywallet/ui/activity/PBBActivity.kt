package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.example.mywallet.R
import com.example.mywallet.ui.fragment.DialogFragment.ListCityFragment
import com.example.mywallet.ui.fragment.DialogFragment.ListProvinceFragment
import com.example.mywallet.ui.fragment.DialogFragment.YearPbbFragment

import com.example.mywallet.di.dbModule
import com.example.mywallet.di.repositoryModule
import com.example.mywallet.di.uiModule
import com.example.mywallet.model.Bill
import com.example.mywallet.model.CheckBillRequest
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants
import com.example.mywallet.viewmodel.TransactionViewModel
import kotlinx.android.synthetic.main.activity_p_b_b.*

import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin


class PBBActivity : AppCompatActivity() {

    var current_id_province: Int = 0
    var current_id_city: Int = 0
    var current_year: Int = 0

    var current_name_province: String? = "0"
    var current_name_city: String? = "0"

    val bundle = Bundle()

    private val viewModel: TransactionViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_p_b_b)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        button_check_bill.isClickable = false
        button_check_bill.isEnabled = false
        edit_text_input_object_number.addTextChangedListener(textWatcher)
        button_select_year.addTextChangedListener(textWatcher)
        button_select_city.addTextChangedListener(textWatcher)
        button_select_province.addTextChangedListener(textWatcher)

        onClickGroup()

        readFieldData()
    }

    fun readFieldData() {
        val preferences = getSharedPreferences("PreferenceApp", 0)
        val selected_province = preferences.getString("selected_province", "")
        current_name_province = preferences.getString("selected_province", "")
        current_id_province = preferences.getInt("current_id_province", 0)
        val selected_city = preferences.getString("selected_city", "")
        current_name_city = preferences.getString("selected_city", "")
        current_id_city = preferences.getInt("current_id_city", 0)
        val selected_year = preferences.getString("selected_year", "")
        current_year = preferences.getInt("current_year", 0)

        button_select_province.setText(current_name_province.toString())
        button_select_city.setText(current_name_city.toString())
        button_select_year.setText(current_year.toString())
    }


    fun onClickGroup() {

        button_select_province.setOnClickListener {
            val dialogFragment = ListProvinceFragment()
            val ft: FragmentTransaction =
                supportFragmentManager.beginTransaction()
            ft.setCustomAnimations(
                R.animator.anim_slide_up, 0, 0,
                R.animator.anim_slide_down
            )
            ft.replace(R.id.frameLayout2, dialogFragment)
            ft.addToBackStack(null)
            ft.commit()
        }

        button_select_city.setOnClickListener {
            val dialogFragment = ListCityFragment()
            val ft: FragmentTransaction =
                supportFragmentManager.beginTransaction()
            ft.setCustomAnimations(
                R.animator.anim_slide_up, 0, 0,
                R.animator.anim_slide_down
            )
            ft.replace(R.id.frameLayout2, dialogFragment)
            ft.addToBackStack(null)
            ft.commit()
        }
        button_select_year.setOnClickListener {
            val dialogFragment = YearPbbFragment()
            val ft: FragmentTransaction =
                supportFragmentManager.beginTransaction()
            ft.setCustomAnimations(
                R.animator.anim_slide_up, 0, 0,
                R.animator.anim_slide_down
            )
            ft.replace(R.id.frameLayout2, dialogFragment)
            ft.addToBackStack(null)
            ft.commit()
        }

        button_check_bill.setOnClickListener {
            getBill(
                current_id_province,
                current_id_city,
                edit_text_input_object_number.toString(),
                current_year
            )
            val intent = Intent(this, CheckoutActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    private val textWatcher: TextWatcher = object : TextWatcher {
        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) { //after text changed
        }

        override fun beforeTextChanged(
            s: CharSequence, start: Int, count: Int,
            after: Int
        ) {
        }

        override fun afterTextChanged(s: Editable) {
            var number = edit_text_input_object_number.text.toString()
            getBill(
                current_id_province,
                current_id_city,
                number,
                current_year
            )
        }
    }

    fun getBill(idProvince: Int, idCity: Int, taxNumber: String, year: Int) {
        val preferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
        val userId = preferences.getInt(Constants.PREF_KEY_USER_ID, 0)
        val token = preferences.getString(Constants.PREF_KEY_TOKEN, "")
        val checkBillRequest = CheckBillRequest(taxNumber, idCity, idProvince, year)
        viewModel.checkBill(
            userId,
            token.toString(),
            checkBillRequest,
            object : MyWalletRepository.getBillCallback {
                override fun onSuccess(bill: Bill, message: String) {
                    val preferences = getSharedPreferences("PreferenceApp", 0)

                    if (bill.provinceName == current_name_province &&
                        bill.year.toString() == current_year.toString() && bill.taxNumber == taxNumber
                    ) {
                        button_check_bill.setBackgroundResource(R.drawable.button_cek_tagihan_background_primary)
                        button_check_bill.setTextColor(resources.getColor(R.color.colorWhite))
                        button_check_bill?.isEnabled = true
                        button_check_bill?.isClickable = true
                        warning_input.visibility = View.GONE
                    } else {
                        button_check_bill.setBackgroundResource(R.drawable.button_cek_tagihan_background_inactive)
                        button_check_bill.setTextColor(resources.getColor(R.color.colorButtonTextInactive))
                        button_check_bill?.isEnabled = false
                        button_check_bill?.isClickable = false
                        warning_input.visibility = View.VISIBLE
                    }

                    val editor = preferences.edit()
                    editor.putString(Constants.PREF_KEY_TAX_NUMBER, bill.taxNumber)
                    editor.putString(Constants.PREF_KEY_TAX_PAY_AMOUNT, bill.payAmount.toString())
                    editor.putString(Constants.PREF_KEY_TAX_ADDRESS, bill.address)
                    editor.putString(Constants.PREF_KEY_TAX_GROUND_AREA, bill.groundArea.toString())
                    editor.putString(Constants.PREF_KEY_TAX_CITY_NAME, bill.cityName)
                    editor.putString(Constants.PREF_KEY_TAX_YEAR, bill.year.toString())
                    editor.putString(Constants.PREF_KEY_TAX_LATITUDE, bill.latitude.toString())
                    editor.putString(Constants.PREF_KEY_TAX_LONGITUDE, bill.longitude.toString())
                    editor.putString(
                        Constants.PREF_KEY_TAX_BUILDING_AREA,
                        bill.buildingArea.toString()
                    )
                    editor.putString(Constants.PREF_KEY_TAX_PROVINCE_NAME, bill.provinceName)
                    editor.apply()
                }

                override fun onFailure(message: String) {
                    button_check_bill.setBackgroundResource(R.drawable.button_cek_tagihan_background_inactive)
                    button_check_bill.setTextColor(resources.getColor(R.color.colorButtonTextInactive))
                    button_check_bill?.isEnabled = false
                    button_check_bill?.isClickable = false
                    warning_input.visibility = View.VISIBLE
                    Log.d("error onFailure", message)
                }
            })
    }
}
