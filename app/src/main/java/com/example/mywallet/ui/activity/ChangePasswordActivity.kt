package com.example.mywallet.ui.activity

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.mywallet.R
import com.example.mywallet.model.UpdateUserDataRequest
import com.example.mywallet.model.User
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants.PASSWORD_REGEX
import com.example.mywallet.util.Constants.PREF_KEY_TOKEN
import com.example.mywallet.util.Constants.PREF_KEY_USER_ID
import com.example.mywallet.util.Constants.PREF_NAME
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.AuthViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_change_password.*
import org.koin.android.ext.android.inject

class ChangePasswordActivity : AppCompatActivity() {

    private val viewModel: AuthViewModel by inject()
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupUI()
    }

    private fun setupUI() {
        button_save_password.setOnClickListener {
            if (isAllFieldValid()) {
                user = User(
                    password = edit_text_old_password.text.toString(),
                    newPassword = edit_text_new_password.text.toString(),
                    retryPassword = edit_text_new_password_confirmation.text.toString()
                )

                updatePassword()
            }
        }

        edit_text_old_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_old_password, text_input_layout_old_password, "Password lama")){
                    clearError(text_input_layout_old_password)
                    showValidSign(text_input_layout_old_password)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        edit_text_new_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_new_password, text_input_layout_new_password, "Password baru")){
                    clearError(text_input_layout_new_password)
                    showValidSign(text_input_layout_new_password)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        edit_text_new_password_confirmation.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_new_password_confirmation, text_input_layout_new_password_confirmation, "Konfirmasi Password")){
                    clearError(text_input_layout_new_password_confirmation)
                    showValidSign(text_input_layout_new_password_confirmation)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }


    private fun updatePassword() {
        layout_progress_bar.visibility = View.VISIBLE
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")

        val updateUserDataRequest = UpdateUserDataRequest(
            user.name,
            user.email,
            user.telephone,
            user.photoProfile,
            user.password,
            user.retryPassword,
            user.newPassword
        )

        viewModel.updateUserData(
            updateUserDataRequest,
            idUser,
            token.toString(),
            object : MyWalletRepository.UpdateProfileCallback {
                override fun onSuccess(user: User?, message: String) {
                    toast(this@ChangePasswordActivity, message)
                    finish()
                    layout_progress_bar.visibility = View.GONE
                }

                override fun onFailure(message: String) {
                    toast(this@ChangePasswordActivity, message)
                    layout_progress_bar.visibility = View.GONE
                }
            })
    }

    private fun isFieldValid(
        editText: TextInputEditText,
        textInputLayout: TextInputLayout,
        name: String
    ): Boolean {
        if (editText == edit_text_new_password_confirmation){
            if (edit_text_new_password_confirmation.text.toString() != edit_text_new_password.text.toString()){
                setFieldError(text_input_layout_new_password_confirmation, getString(R.string.error_confirm_password_not_match))
                return false
            }
        }

        if (editText.text.toString().isNotEmpty()) {
            if (!editText.text.toString().matches(PASSWORD_REGEX)) {
                setFieldError(textInputLayout, getString(R.string.error_password_not_valid))
                return false
            }
        } else {
            setFieldError(textInputLayout, getString(R.string.error_field_cannot_empty, name))
            return false
        }

        return true

    }

    private fun setFieldError(textInputLayout: TextInputLayout, message: String) {
        textInputLayout.error = message
    }

    private fun clearError(textInputLayout: TextInputLayout) {
        textInputLayout.error = null
    }

    private fun showValidSign(textInputLayout: TextInputLayout) {
        textInputLayout.boxStrokeColor = resources.getColor(R.color.colorGreenSuccess)
    }

    private fun isAllFieldValid(): Boolean {
        isFieldValid(edit_text_old_password, text_input_layout_old_password, "Password lama")
        isFieldValid(edit_text_new_password, text_input_layout_new_password, "Password baru")
        isFieldValid(
            edit_text_new_password_confirmation,
            text_input_layout_new_password_confirmation,
            "Konfirmasi password"
        )

        return (text_input_layout_old_password.error.isNullOrEmpty()
                && text_input_layout_new_password.error.isNullOrEmpty()
                && text_input_layout_new_password_confirmation.error.isNullOrEmpty())

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }

        return false
    }
}
