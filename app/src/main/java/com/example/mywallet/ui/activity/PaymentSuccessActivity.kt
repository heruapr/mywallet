package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.mywallet.R
import com.example.mywallet.model.User
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants
import com.example.mywallet.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_payment_success.*
import kotlinx.android.synthetic.main.activity_profile.*
import org.koin.android.ext.android.inject

class PaymentSuccessActivity : AppCompatActivity() {
    private val viewModel: AuthViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_success)
        getUpdateBalance()
        btn_finish.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    fun getUpdateBalance() {
        val sharedPreferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
        val idUser = sharedPreferences.getInt(Constants.PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(Constants.PREF_KEY_TOKEN, "")
        val editor = sharedPreferences.edit()
        viewModel.getUserData(
            idUser,
            token.toString(),
            object : MyWalletRepository.GetUserDataCallback {
                override fun onSuccess(user: User?, message: String) {
                    editor.putFloat(Constants.PREF_KEY_USER_BALANCE, user?.balance!!.toFloat())
                    editor.apply()
                }
                override fun onFailure(message: String) {
                }
            })
    }
}
