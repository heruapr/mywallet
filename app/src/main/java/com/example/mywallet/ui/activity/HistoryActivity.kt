package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mywallet.R
import com.example.mywallet.model.Transaction
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.ui.adapter.HistoryAdapter
import com.example.mywallet.util.Constants.BUNDLE_KEY_TAX_BILL
import com.example.mywallet.util.Constants.BUNDLE_KEY_TAX_ID
import com.example.mywallet.util.Constants.BUNDLE_KEY_TAX_VA_NUMBER
import com.example.mywallet.util.Constants.PREF_KEY_TOKEN
import com.example.mywallet.util.Constants.PREF_KEY_USER_ID
import com.example.mywallet.util.Constants.PREF_NAME
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.HistoryViewModel
import kotlinx.android.synthetic.main.activity_history.*
import org.koin.android.ext.android.inject

class HistoryActivity : AppCompatActivity() {

    private val viewModel: HistoryViewModel by inject()
    private val adapter: HistoryAdapter by inject()
    private lateinit var sharedPreferences: SharedPreferences
    val bundle = Bundle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        getAllTransaction()
    }

    private fun setupRecyclerView(transactionList: ArrayList<Transaction>) {
        adapter.setList(transactionList.reversed() as ArrayList<Transaction>)
        adapter.setListener(object : HistoryAdapter.HistoryListener {
            override fun onItemClicked(transaction: Transaction) {
                if (transaction.isFinished == 0) {
                    //INTENT TO TRANSACTION DETAIL
                    val intent = Intent(this@HistoryActivity, WaitingforPaymentActivity::class.java)
                    bundle.putString(BUNDLE_KEY_TAX_ID, transaction.idTransaction)
                    bundle.putString(BUNDLE_KEY_TAX_BILL, transaction.amount.toString())
                    bundle.putString(BUNDLE_KEY_TAX_VA_NUMBER, transaction.vaNumber)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }
        })
        recycler_view_history.apply {
            layoutManager = LinearLayoutManager(this@HistoryActivity, RecyclerView.VERTICAL, false)
            adapter = this@HistoryActivity.adapter
        }
    }

    private fun getAllTransaction() {
        showProgressBar(true)
        val userId = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")

        viewModel.getAllTransaction(
            userId,
            token.toString(),
            object : MyWalletRepository.GetAllTransactionCallback {
                override fun onSuccess(transactionList: List<Transaction>?, message: String) {
                    val list = arrayListOf<Transaction>()
                    list.addAll(transactionList!!)
                    setupRecyclerView(list)
                    toast(this@HistoryActivity, message)
                    showProgressBar(false)
                }

                override fun onFailure(message: String) {
                    toast(this@HistoryActivity, message)
                }
            })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return false
    }

    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            layout_progress_bar.visibility = View.VISIBLE
        } else {
            layout_progress_bar.visibility = View.GONE
        }
    }
}
