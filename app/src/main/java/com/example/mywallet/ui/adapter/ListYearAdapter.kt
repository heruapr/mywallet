package com.example.mywallet.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.example.mywallet.ui.activity.PBBActivity
import com.example.mywallet.R

class ListYearAdapter : RecyclerView.Adapter<ListYearAdapter.ListYearHolder>() {
    private var listYear: List<com.example.mywallet.db.entity.Year> = ArrayList()
    //    var listYear = MutableLiveData<MutableList<Year>>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListYearHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_year, parent, false)
        return ListYearHolder(itemView)
    }
    override fun getItemCount(): Int {
        return listYear.size
    }

    override fun onBindViewHolder(holder: ListYearHolder, position: Int) {
        val currentYear = listYear[position]
//        holder.textViewPulsa.text = currentPulsa.nominal.toString()
        holder.textViewYear.text = currentYear.year.toString()
        holder.textViewYear.setOnClickListener {
            val activity = it.getContext() as PBBActivity
            val ft: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
            ft.setCustomAnimations(R.animator.anim_slide_up, 0, 0, R.animator.anim_slide_down)
            //ft.replace(R.id.frameLayout, dialogFragment)
            activity.onBackPressed()
            val preferences = activity.getSharedPreferences("PreferenceApp", 0)
            val editor = preferences.edit()
            editor.putString("selected_year", currentYear.year.toString())
            editor.putInt("current_year", currentYear.year)
            editor.apply()
            activity.readFieldData()
            ft.commit()
        }

    }

    inner class ListYearHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewYear: TextView = itemView.findViewById(R.id.text_view_item_year)
    }

    fun setYear(listYear: List<com.example.mywallet.db.entity.Year>) {
        this.listYear = listYear
        notifyDataSetChanged()
    }
}