package com.example.mywallet.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.mywallet.R
import com.example.mywallet.model.LogoutRequest
import com.example.mywallet.model.UpdateUserDataRequest
import com.example.mywallet.model.User
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants
import com.example.mywallet.util.Constants.BASE_URL
import com.example.mywallet.util.Constants.PREF_KEY_IS_LOGIN
import com.example.mywallet.util.Constants.PREF_KEY_PHOTO_URL
import com.example.mywallet.util.Constants.PREF_KEY_TOKEN
import com.example.mywallet.util.Constants.PREF_KEY_USER_EMAIL
import com.example.mywallet.util.Constants.PREF_KEY_USER_ID
import com.example.mywallet.util.Constants.PREF_KEY_USER_NAME
import com.example.mywallet.util.Constants.PREF_KEY_USER_PHONE
import com.example.mywallet.util.Constants.PREF_NAME
import com.example.mywallet.util.Constants.TAG
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_profile.*
import org.koin.android.ext.android.inject
import kotlin.math.roundToInt

class ProfileActivity : AppCompatActivity() {

    companion object {
        private const val PIN_VERIFICATION_REQUEST = 99
        private const val PIN_EDIT_REQUEST = 98
    }

    private val viewModel: AuthViewModel by inject()
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupUI()
    }

    private fun setupUI() {

        //FOR DEBUGGING PURPOSE ONLY
        text_view_token.text = "token-DEBUG : ${sharedPreferences.getString(
            PREF_KEY_TOKEN,
            ""
        )}(${sharedPreferences.getInt(
            PREF_KEY_USER_ID, 0
        )})"
        checkEmailStatus()
        text_view_name_profile.text = sharedPreferences.getString(PREF_KEY_USER_NAME, "")
        text_view_email_profile.text = sharedPreferences.getString(PREF_KEY_USER_EMAIL, "")
        text_view_phone_profile.text = sharedPreferences.getString(PREF_KEY_USER_PHONE, "")
        text_view_balance_profile.text =
            "Rp. ${sharedPreferences.getFloat(Constants.PREF_KEY_USER_BALANCE, 0f).roundToInt()}"
        text_view_logout.setOnClickListener {
            logout()
        }
        text_view_history.setOnClickListener {
            val intent = Intent(this, HistoryActivity::class.java)
            startActivity(intent)
        }
        text_view_edit_profile.setOnClickListener {
            val intent = Intent(this, EditProfileActivity::class.java)
            startActivity(intent)
        }
        text_view_edit_pin.setOnClickListener {
            val intent = Intent(this, PinActivity::class.java)
            intent.putExtra(PinActivity.TYPE_EXTRA, "verification")
            startActivityForResult(intent, PIN_VERIFICATION_REQUEST)
        }
        text_view_edit_phone.setOnClickListener {
            val intent = Intent(this, EditPhoneActivity::class.java)
            startActivity(intent)
        }
        text_view_edit_password.setOnClickListener {
            val intent = Intent(this, ChangePasswordActivity::class.java)
            startActivity(intent)
        }

        text_view_verify_email.setOnClickListener {
            val user = User(email = sharedPreferences.getString(PREF_KEY_USER_EMAIL, ""))
            verifyEmail(user)
        }

        val photo = sharedPreferences.getString(PREF_KEY_PHOTO_URL, "")
        if (photo != "") {
            Glide.with(this).load("${BASE_URL}/$photo").into(image_view_avatar_profile)
        }
    }

    private fun checkEmailStatus() {
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")
        val editor = sharedPreferences.edit()
        //test update balance


        viewModel.getUserData(
            idUser,
            token.toString(),
            object : MyWalletRepository.GetUserDataCallback {
                override fun onSuccess(user: User?, message: String) {
                    if (user?.isEmailActive == 0) {
                        layout_email_unverified.visibility = View.VISIBLE
                        editor.putFloat(Constants.PREF_KEY_USER_BALANCE, user?.balance.toFloat())
                        editor.apply()
                    } else {
                        layout_email_unverified.visibility = View.GONE
                    }
                }

                override fun onFailure(message: String) {
                }
            })
    }

    private fun logout() {
        showProgressBar(true)
        val logoutRequest = LogoutRequest(
            sharedPreferences.getInt(PREF_KEY_USER_ID, 0),
            sharedPreferences.getString(PREF_KEY_TOKEN, "") as String
        )


        Log.d(TAG, "token = ${sharedPreferences.getString(PREF_KEY_TOKEN, "")}")
        viewModel.logout(
            logoutRequest.idUser,
            logoutRequest.token,
            object : MyWalletRepository.LogoutCallback {
                override fun onSuccess(message: String) {
                    toast(this@ProfileActivity, message)
                    val intent = Intent(this@ProfileActivity, LoginRegisterActivity::class.java)
                    startActivity(intent)
                    finishAffinity()
                    showProgressBar(false)
                    updatePref()
                }

                override fun onFailure(message: String) {
                    toast(this@ProfileActivity, message)
                    showProgressBar(false)
                }
            })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return false
    }

    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            layout_progress_bar.visibility = View.VISIBLE
        } else {
            layout_progress_bar.visibility = View.GONE
        }
    }

    private fun verifyEmail(user: User) {
        layout_progress_bar.visibility = View.VISIBLE
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")

        val updateUserDataRequest = UpdateUserDataRequest(
            user.name,
            user.email,
            user.telephone,
            user.photoProfile,
            user.password,
            user.newPassword,
            user.retryPassword
        )

        viewModel.updateUserData(
            updateUserDataRequest,
            idUser,
            token.toString(),
            object : MyWalletRepository.UpdateProfileCallback {
                override fun onSuccess(user: User?, message: String) {
                    toast(
                        this@ProfileActivity,
                        "Email verifikasi telah dikirimkan ke ${user?.email}"
                    )
                    layout_progress_bar.visibility = View.GONE
                }

                override fun onFailure(message: String) {
                    toast(this@ProfileActivity, message)
                    layout_progress_bar.visibility = View.GONE
                }
            })
    }

    private fun updatePref() {
        val editor = sharedPreferences.edit()
        editor.putBoolean(PREF_KEY_IS_LOGIN, false)
        editor.apply()
        editor.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PIN_VERIFICATION_REQUEST -> {
                    val intent = Intent(this, PinActivity::class.java)
                    intent.putExtra(PinActivity.TYPE_EXTRA, "edit")
                    startActivityForResult(intent, PIN_EDIT_REQUEST)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        setupUI()
    }
}
