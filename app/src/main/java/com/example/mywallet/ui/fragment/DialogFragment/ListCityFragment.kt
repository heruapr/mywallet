package com.example.mywallet.ui.fragment.DialogFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.mywallet.R
import com.example.mywallet.ui.adapter.ListCityAdapter
import com.example.mywallet.model.City
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants
import com.example.mywallet.viewmodel.CityViewModel
import kotlinx.android.synthetic.main.fragment_list_city.*
import kotlinx.android.synthetic.main.fragment_year_pbb.button_close
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class ListCityFragment() : Fragment() {
    private val adapter: ListCityAdapter by inject()
    private val cityViewModel: CityViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_city, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        city_recyclerview.layoutManager = LinearLayoutManager(this.requireContext())
        city_recyclerview.setHasFixedSize(true)
        city_recyclerview.adapter = adapter

        val preferences = requireActivity().getSharedPreferences("PreferenceApp", 0)
        val idProvince = preferences.getInt("current_id_province", 0)

        getCity(idProvince)

        button_close.setOnClickListener {
            requireActivity().onBackPressed()
        }
        edit_text_search_city.setOnClickListener {
        }
    }

    private fun setupRecyclerView(list: ArrayList<City>) {
        adapter.setList(list)
        city_recyclerview.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = this.adapter
        }
    }

    fun getCity(idProvince: Int) {
        showProgressBar(true)
        cityViewModel.getListCity(idProvince, object : MyWalletRepository.GetListCityCallback {
            override fun onSuccess(
                cityList: List<City>,
                message: String
            ) {
                showProgressBar(false)
                val list = arrayListOf<City>()
                list.addAll(cityList)
                setupRecyclerView(list)
                Constants.toast(requireContext(), message)
            }

            override fun onFailure(message: String) {
                showProgressBar(false)
                Constants.toast(requireContext(), message)
            }
        })
    }

    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            progress_bar.visibility = View.VISIBLE
        } else {
            progress_bar.visibility = View.GONE
        }
    }
}

