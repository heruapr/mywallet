package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mywallet.R
import com.example.mywallet.ui.adapter.ListBankAdapter
import com.example.mywallet.db.entity.Bank
import com.example.mywallet.rest.ApiClient
import com.example.mywallet.util.Constants
import com.example.mywallet.viewmodel.BankViewModel
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_payment.*
import org.json.JSONObject
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.roundToInt

class PaymentActivity : AppCompatActivity() {
    private val adapter: ListBankAdapter by inject()
    private val bankViewModel: BankViewModel by inject()
    var payAmount: String = ""
    var taxNumber: String = ""
    var year: String = ""
    val bundle = Bundle()


    override fun onCreate(savedInstanceState: Bundle?) {
        val preferences = getSharedPreferences("PreferenceApp", 0)
        val sharedPreferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        textview_balance.setText(sharedPreferences.getFloat(Constants.PREF_KEY_USER_BALANCE,0f).toString())
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
            textview_total.setText("Rp." + preferences.getString(Constants.PREF_KEY_TAX_PAY_AMOUNT,"").toString())
            textview_balance.setText(sharedPreferences.getFloat(Constants.PREF_KEY_USER_BALANCE, 0f).roundToInt().toString())
            taxNumber = preferences.getString(Constants.PREF_KEY_TAX_NUMBER,"").toString()
            payAmount = preferences.getString(Constants.PREF_KEY_TAX_PAY_AMOUNT,"").toString()
            year = preferences.getString(Constants.PREF_KEY_TAX_YEAR,"").toString()

        onClickGroup()
        //setupRecyclerView()
        getBank()
    }


    fun onClickGroup() {
        card_wallet.setOnClickListener {
            val intent = Intent(this, WalletPaymentActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setupRecyclerView() {
        showProgressBar(false)
        bank_recycler_view.layoutManager = LinearLayoutManager(this)
        bank_recycler_view.setHasFixedSize(true)
        bank_recycler_view.adapter = adapter
        bankViewModel.getAllBanks().observe(this,
            Observer<List<Bank>> { list ->
                list?.let {
                    adapter.setBank(it)
                }
            })
        Log.d("SAMPAI OBSERVER", "sampai OBSERVER")
    }

    fun getBank() {
        ApiClient().services.getBank()
            .enqueue(object : Callback<JsonObject> {
                override fun onFailure(
                    call: Call<JsonObject>,
                    t: Throwable
                ) {
                    Log.d(Constants.TAG, "error getBank 1 : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {

                    if (response.isSuccessful) {
                        val body = response.body()
                        if(JSONObject(body.toString()).getInt("status").equals(200)) {
                            writeData(body.toString())
                        }else{
                            Log.d(Constants.TAG,"error server")
                        }
                        Log.d(Constants.TAG, "MANTAP bank : $body")
                    } else {
                        Log.d(Constants.TAG, "getting bank error :")
                    }
                }
            })
    }

    fun writeData(response: String) {
        showProgressBar(true)
        val obj = JSONObject(response).getJSONObject("data")
        val dataArray = obj.getJSONArray("bank")
        Log.d("WRITE DATA", "sampai WRITE , lengt array obj: ${dataArray.length()}")
        for (i in 0..dataArray.length() - 1) {
            val dataobj = dataArray.getJSONObject(i)
            //save to sqlite
            Log.d("DATA PROV 1111", dataobj.getString("bankName").toString())
            Log.d("DATA PROV 2222", dataobj.getString("idBank").toString())
            bankViewModel.insert(
                Bank(
                    dataobj.getString("idBank").toInt(),
                    dataobj.getString("bankName").toString(),
                    dataobj.getString("vaCode").toInt()
                )
            )
            Log.d("SIMPAANN", "SAVED DATA")
        }
        setupRecyclerView()
    }
    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            progress_bar.visibility = View.VISIBLE
        } else {
            progress_bar.visibility = View.GONE
        }
    }
}






