package com.example.mywallet.ui.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.mywallet.R
import com.example.mywallet.util.Constants.EMAIL_REGEX
import com.example.mywallet.util.Constants.PASSWORD_REGEX
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun setupUI() {
        edit_text_email_login.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_email_login, text_input_layout_email, "Email")) {
                    clearError(text_input_layout_email)
                    showValidSign(text_input_layout_email)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        edit_text_password_login.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(
                        edit_text_password_login,
                        text_input_layout_password,
                        "Password"
                    )
                ) {
                    clearError(text_input_layout_password)
                    showValidSign(text_input_layout_password)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun isFieldValid(
        editText: TextInputEditText,
        textInputLayout: TextInputLayout,
        name: String
    ): Boolean {
        if (editText.text.toString().isNotEmpty()) {
            when (editText) {
                edit_text_email_login -> {
                    if (!editText.text.toString().matches(EMAIL_REGEX)) {
                        setFieldError(textInputLayout, getString(R.string.error_email_not_valid))
                        return false
                    }
                }

                edit_text_password_login -> {
                    if (!editText.text.toString().matches(PASSWORD_REGEX)) {
                        setFieldError(textInputLayout, getString(R.string.error_password_not_valid))
                        return false
                    }
                }
            }
        } else {
            setFieldError(textInputLayout, getString(R.string.error_field_cannot_empty, name))
            return false
        }

        return true

    }


    private fun setFieldError(textInputLayout: TextInputLayout, message: String) {
        textInputLayout.error = message
    }

    private fun clearError(textInputLayout: TextInputLayout) {
        textInputLayout.error = null
    }

    private fun showValidSign(textInputLayout: TextInputLayout) {
        textInputLayout.boxStrokeColor = resources.getColor(R.color.colorGreenSuccess)
    }

    fun isAllFieldValid(): Boolean {
        isFieldValid(edit_text_email_login, text_input_layout_email, "Email")
        isFieldValid(edit_text_password_login, text_input_layout_password, "Password")
        return text_input_layout_email.error.isNullOrEmpty() && text_input_layout_password.error.isNullOrEmpty()
    }


}
