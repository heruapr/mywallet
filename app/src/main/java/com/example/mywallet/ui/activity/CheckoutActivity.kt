package com.example.mywallet.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.mywallet.R
import com.example.mywallet.ui.fragment.MapFragment
import com.example.mywallet.util.Constants
import kotlinx.android.synthetic.main.activity_checkout.*


class CheckoutActivity : AppCompatActivity() {
    var lat: String = ""
    var long: String = ""
    var payAmount: String = ""
    var taxNumber: String = ""
    var year: String = ""
    val bundle = Bundle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val preferences = getSharedPreferences("PreferenceApp", 0)
        onClickGroup()

        textview_merchant.setText(preferences.getString(Constants.PREF_KEY_TAX_CITY_NAME, ""))
        textview_address.setText(preferences.getString(Constants.PREF_KEY_TAX_ADDRESS, ""))
        textview_area_wide_scale.setText(
            preferences.getString(
                Constants.PREF_KEY_TAX_GROUND_AREA,
                ""
            )
        )
        textview_bill.setText("Rp." + preferences.getString(Constants.PREF_KEY_TAX_PAY_AMOUNT, ""))
        textview_building_wide_scale.setText(
            preferences.getString(
                Constants.PREF_KEY_TAX_GROUND_AREA,
                ""
            )
        )
        textview_long.setText(preferences.getString(Constants.PREF_KEY_TAX_LONGITUDE, ""))
        textview_lat.setText(preferences.getString(Constants.PREF_KEY_TAX_LATITUDE, ""))
        textview_object_number.setText(preferences.getString(Constants.PREF_KEY_TAX_NUMBER, ""))
        textview_year.setText(preferences.getString(Constants.PREF_KEY_TAX_YEAR, ""))

        lat = preferences.getString(Constants.PREF_KEY_TAX_LATITUDE, "").toString()
        long = preferences.getString(Constants.PREF_KEY_TAX_LONGITUDE, "").toString()
        taxNumber = preferences.getString(Constants.PREF_KEY_TAX_NUMBER, "").toString()
        payAmount = preferences.getString(Constants.PREF_KEY_TAX_PAY_AMOUNT, "").toString()
        year = preferences.getString(Constants.PREF_KEY_TAX_YEAR, "").toString()

    }

    fun onClickGroup() {

        button_map.setOnClickListener {
            frameLayout2.bringToFront()
            var lat: String = lat
            var lang: String = long
            val bundle = Bundle()
            bundle.putString("lat", lat)
            bundle.putString("lang", lang)
            val mapFragment = MapFragment()
            mapFragment.setArguments(bundle)
            val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
            ft.setCustomAnimations(
                R.animator.anim_slide_up, 0, 0,
                R.animator.anim_slide_down
            )
            ft.replace(R.id.frameLayout2, mapFragment)
            ft.addToBackStack(null)
            ft.commit()
        }

        button_pay.setOnClickListener {
            val intent = Intent(this, PaymentActivity::class.java)
            bundle.putString("taxNumber", taxNumber)
            bundle.putString("payAmount", payAmount)
            bundle.putString("year", year)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }
}
