package com.example.mywallet.ui.activity

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.example.mywallet.R
import com.example.mywallet.model.UpdateUserDataRequest
import com.example.mywallet.model.User
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants
import com.example.mywallet.util.Constants.BASE_URL
import com.example.mywallet.util.Constants.NAME_REGEX
import com.example.mywallet.util.Constants.PREF_KEY_PHOTO_URL
import com.example.mywallet.util.Constants.PREF_KEY_TOKEN
import com.example.mywallet.util.Constants.PREF_KEY_USER_EMAIL
import com.example.mywallet.util.Constants.PREF_KEY_USER_ID
import com.example.mywallet.util.Constants.PREF_KEY_USER_NAME
import com.example.mywallet.util.Constants.PREF_NAME
import com.example.mywallet.util.Constants.TAG
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.AuthViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.format
import id.zelory.compressor.constraint.quality
import id.zelory.compressor.constraint.resolution
import id.zelory.compressor.constraint.size
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.koin.android.ext.android.inject
import java.io.File
import java.io.IOException


class EditProfileActivity : AppCompatActivity() {

    companion object {
        private const val PICK_IMAGE_PERMISSION = 99
        private const val TAKE_PHOTO_REQUEST_CODE = 0
        private const val PICK_IMAGE_REQUEST_CODE = 1
    }

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var currentPhotoPath: String
    private var photoUri: Uri? = null
    private val viewModel: AuthViewModel by inject()
    private var photoFile: File? = null
    private var compressedPhotoFile: File? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        setupUI()
    }

    private fun setupUI() {
        edit_text_name_edit_profile.setText(sharedPreferences.getString(PREF_KEY_USER_NAME, ""))
        edit_text_email_edit_profile.setText(sharedPreferences.getString(PREF_KEY_USER_EMAIL, ""))
        button_change_profile_picture.setOnClickListener {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED) {
                    askPermission()
                } else {
                    selectImageDialog()
                }
            } else {
                selectImageDialog()
            }
        }
        button_save_profile.setOnClickListener {
            if (isAllFieldValid()) {
                val newName = edit_text_name_edit_profile.text.toString()
                val oldEmail = sharedPreferences.getString(PREF_KEY_USER_EMAIL, "")
                val newEmail =
                    if (oldEmail.equals(edit_text_email_edit_profile.text.toString(), false)) {
                        null
                    } else {
                        edit_text_email_edit_profile.text.toString()
                    }

                val user = User(
                    name = newName,
                    email = newEmail
                )

                Log.d(TAG, "photo URI = $photoUri")
                if (photoFile != null) {
                    uploadPhoto(user)
                } else {
                    updateProfile(user)
                }
            }
        }

        val photo = sharedPreferences.getString(PREF_KEY_PHOTO_URL, "")
        if (photo != "") {
            Glide.with(this).load("${BASE_URL}$photo").into(image_view_avatar_profile)
        }

        edit_text_name_edit_profile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_name_edit_profile, text_input_layout_name, "Nama")){
                    clearError(text_input_layout_name)
                    showValidSign(text_input_layout_name)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        }
        )

        edit_text_email_edit_profile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_email_edit_profile, text_input_layout_email, "Email")){
                    clearError(text_input_layout_email)
                    showValidSign(text_input_layout_email)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        }
        )

    }

    private fun selectImageDialog() {
        val options =
            arrayOf("Take Photo", "Choose from Gallery", "Cancel")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Choose your profile picture")

        builder.setItems(options) { dialog, item ->
            when {
                options[item] == "Take Photo" -> {
                    takePicture()
                }
                options[item] == "Choose from Gallery" -> {
                    chooseFromGallery()
                }
                options[item] == "Cancel" -> {
                    dialog.dismiss()
                }
            }
        }
        builder.show()
    }

    private fun takePicture() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    photoUri = FileProvider.getUriForFile(
                        this,
                        "com.example.mywallet.fileprovider",
                        it
                    )

                    Log.d(TAG, "photo URI 1 = $photoUri")
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                    startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST_CODE)
                }
            }
        }
    }

    private fun createImageFile(): File {
        // Create an image file name
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES) as File
        return File.createTempFile(
            "my_wallet_profile", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
            Log.d(TAG, "current photo path = $currentPhotoPath")

        }
    }

    private fun chooseFromGallery() {
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(pickPhoto, PICK_IMAGE_REQUEST_CODE)
    }

    private fun getRealPathFromURI(photoUri: Uri): String {
        var cursor: Cursor? = null;
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = applicationContext.contentResolver.query(photoUri, proj, null, null, null)
            val columnIndex = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor?.moveToFirst();
            return cursor?.getString(columnIndex!!.toInt()).toString()
        } finally {
            cursor?.close()
        }
    }

    @TargetApi(23)
    private fun askPermission() {
        val permissions = arrayOf(
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.CAMERA"
        )

        requestPermissions(permissions, PICK_IMAGE_PERMISSION)
    }

    private fun updateProfile(userEdit: User) {
        layout_progress_bar.visibility = View.VISIBLE
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")

        val updateUserDataRequest = UpdateUserDataRequest(
            userEdit.name,
            userEdit.email,
            userEdit.telephone,
            userEdit.photoProfile,
            userEdit.password,
            userEdit.newPassword,
            userEdit.retryPassword
        )

        viewModel.updateUserData(
            updateUserDataRequest,
            idUser,
            token.toString(),
            object : MyWalletRepository.UpdateProfileCallback {
                override fun onSuccess(user: User?, message: String) {
                    updateUserPrefData(user)
                    toast(this@EditProfileActivity, message)
                    finish()
                    layout_progress_bar.visibility = View.GONE

                }

                override fun onFailure(message: String) {
                    toast(this@EditProfileActivity, message)
                    layout_progress_bar.visibility = View.GONE
                }
            })
    }

    private fun uploadPhoto(userEdit: User) {
        layout_progress_bar.visibility = View.VISIBLE
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")

        compressImage()
        val requestBody = RequestBody.create(MediaType.parse("image/jpg"), compressedPhotoFile!!)
        val filePart = MultipartBody.Part.createFormData("photo", compressedPhotoFile?.name, requestBody)
        Log.d(
            TAG,
            "File ${compressedPhotoFile}\n${compressedPhotoFile?.absoluteFile}\n${compressedPhotoFile?.absolutePath}\n${compressedPhotoFile?.name}\n${compressedPhotoFile?.path}"
        )

        viewModel.uploadPhoto(
            filePart,
            idUser,
            token.toString(),
            object : MyWalletRepository.UploadPhotoCallback {
                override fun onSuccess(user: User?, message: String) {
                    updateProfile(userEdit)
                    toast(this@EditProfileActivity, message)

                }

                override fun onFailure(message: String) {
                    toast(this@EditProfileActivity, message)
                    layout_progress_bar.visibility = View.GONE
                }
            })
    }

    private fun compressImage() {

        val deferred = GlobalScope.async {
            Compressor.compress(applicationContext, photoFile!!) {
                resolution(1024, 768)
                quality(80)
                format(Bitmap.CompressFormat.JPEG)
                size(500_000) // 500 KB
            }
        }
        runBlocking {
            compressedPhotoFile = deferred.await()
        }

    }

    private fun updateUserPrefData(user: User?) {
        val editor = sharedPreferences.edit()
        editor.putString(PREF_KEY_USER_NAME, user?.name)
        editor.putString(PREF_KEY_USER_EMAIL, user?.email)
        editor.putString(PREF_KEY_PHOTO_URL, user?.photoProfile)
        editor.apply()
        editor.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                TAKE_PHOTO_REQUEST_CODE -> {
                    val imageUri = data?.data
                    photoFile = File(currentPhotoPath)
                    Log.d(TAG, "photoUri res = $photoUri")
                    image_view_avatar_profile.setImageURI(photoUri)
                    Glide.with(this).load(photoUri).into(image_view_avatar_profile)

                }

                PICK_IMAGE_REQUEST_CODE -> {
                    val photoUri = data?.data
                    val realPath = getRealPathFromURI(photoUri!!)

                    photoFile = File(realPath)
                    Log.d(TAG, "photoUri res = $photoUri")
                    image_view_avatar_profile.setImageURI(photoUri)
                    Glide.with(this).load(photoUri).into(image_view_avatar_profile)

                }
            }
        }
    }

    private fun isFieldValid(
        editText: TextInputEditText,
        textInputLayout: TextInputLayout,
        name: String
    ): Boolean {
        if (editText.text.toString().isNotEmpty()) {
            when (editText) {
                edit_text_name_edit_profile -> {
                    if (!editText.text.toString().matches(NAME_REGEX)) {
                        setFieldError(textInputLayout, getString(R.string.error_name_not_valid))
                        return false
                    }
                }
                edit_text_email_edit_profile -> {
                    if (!editText.text.toString().matches(Constants.EMAIL_REGEX)) {
                        setFieldError(textInputLayout, getString(R.string.error_email_not_valid))
                        return false
                    }
                }
            }
        } else {
            setFieldError(textInputLayout, getString(R.string.error_field_cannot_empty, name))
            return false
        }

        return true

    }

    private fun setFieldError(textInputLayout: TextInputLayout, message: String) {
        textInputLayout.error = message
    }

    private fun clearError(textInputLayout: TextInputLayout) {
        textInputLayout.error = null
    }

    private fun showValidSign(textInputLayout: TextInputLayout) {
        textInputLayout.boxStrokeColor = resources.getColor(R.color.colorGreenSuccess)
    }

    private fun isAllFieldValid(): Boolean {
        isFieldValid(edit_text_email_edit_profile, text_input_layout_name, "Nama")
        isFieldValid(edit_text_email_edit_profile, text_input_layout_email, "Email")

        return (text_input_layout_name.error.isNullOrEmpty() && text_input_layout_email.error.isNullOrEmpty())
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                PICK_IMAGE_PERMISSION -> {
                    selectImageDialog()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }

        return false
    }
}
