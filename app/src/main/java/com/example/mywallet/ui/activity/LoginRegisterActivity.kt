package com.example.mywallet.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.mywallet.R
import com.example.mywallet.model.LoginRequest
import com.example.mywallet.model.RegisterRequest
import com.example.mywallet.model.User
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.ui.adapter.LoginRegisterAdapter
import com.example.mywallet.ui.fragment.LoginFragment
import com.example.mywallet.ui.fragment.RegisterFragment
import com.example.mywallet.util.Constants
import com.example.mywallet.util.Constants.PREF_KEY_IS_LOGIN
import com.example.mywallet.util.Constants.PREF_NAME
import com.example.mywallet.util.Constants.TAG
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_login_register.*
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_register.*
import org.koin.android.ext.android.inject


class LoginRegisterActivity : AppCompatActivity() {

    companion object {
        private const val RESEND_OTP_REQUEST = 99
        const val PHONE_NUMBER_EXTRA = "phone_number_extra"
        const val USER_ID_EXTRA = "user_id_extra"
    }

    private val loginFragment = LoginFragment()
    private val registerFragment = RegisterFragment()


    private lateinit var adapter: LoginRegisterAdapter
    private val viewModel: AuthViewModel by inject()
    private lateinit var sharedPreferences: SharedPreferences

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_register)
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        window.statusBarColor = Color.TRANSPARENT
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        setupUI()
    }

    private fun setupUI() {

        button_login_register.setOnClickListener {
            if (button_login_register.text == resources.getString(R.string.login)) {

                Log.d(TAG, "is all field valid = ${loginFragment.isAllFieldValid()}")
                if (loginFragment.isAllFieldValid()){
                    login(
                        loginFragment.edit_text_email_login.text.toString(),
                        loginFragment.edit_text_password_login.text.toString()
                    )
                }
//                if (isLoginFieldValid()) {
////                    startActivity(Intent(this, MainActivity::class.java))
//                    login(
//                        loginFragment.edit_text_email_login.text.toString(),
//                        loginFragment.edit_text_password_login.text.toString()
//                    )
//                }

            } else {
                if (registerFragment.isAllFieldValid()) {
//                    val intent = Intent(this, VerificationCodeActivity::class.java)
//                    intent.putExtra(
//                        VerificationCodeActivity.USER_DATA_EXTRA,
//                        User(idUser = 9, telephone = "+6282114690823")
//                    )
//                    startActivity(intent)
                    register(
                        registerFragment.edit_text_name_register.text.toString(),
                        registerFragment.edit_text_email_register.text.toString(),
                        registerFragment.edit_text_phone_register.text.toString(),
                        registerFragment.edit_text_password_register.text.toString(),
                        registerFragment.edit_text_password_confirmation_register.text.toString()
                    )
                }

            }
        }
        adapter = LoginRegisterAdapter(
            supportFragmentManager
        )
        adapter.addFragment(loginFragment, "Login")
        adapter.addFragment(registerFragment, "Register")

        view_pager_login_register.adapter = adapter
        tab_layout_login_register.setViewPager(view_pager_login_register)

        tab_layout_login_register.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        button_login_register.setText(R.string.login)

                    }
                    1 -> {
                        button_login_register.setText(R.string.register)
                    }
                }
            }
        })
    }

    private fun login(email: String, password: String) {
        Log.d(TAG, "login method")
        showProgressBar(true)
        val loginRequest = LoginRequest(email, password)
        viewModel.login(loginRequest, object : MyWalletRepository.LoginCallback {
            override fun onSuccess(user: User?, message: String) {
                Log.d(TAG, "user = $user")
                val intent = Intent(this@LoginRegisterActivity, MainActivity::class.java)
                intent.putExtra(MainActivity.USER_DATA_EXTRA, user)
                startActivity(intent)
                finish()
                toast(this@LoginRegisterActivity, message)
                showProgressBar(false)
                saveUserPreference(user)
            }

            override fun onFailure(message: String) {
                toast(this@LoginRegisterActivity, message)
                showProgressBar(false)

                if (message.equals(Constants.API_ACCOUNT_NOT_VERIFIED_MESSAGE, true)) {
                    val intent =
                        Intent(this@LoginRegisterActivity, PinActivity::class.java)

                    intent.putExtra(PinActivity.TYPE_EXTRA, "otp")

                    startActivityForResult(intent, RESEND_OTP_REQUEST)
                }
            }
        })
    }

    private fun register(
        name: String,
        email: String,
        phone: String,
        password: String,
        retryPassword: String
    ) {
        Log.d(TAG, "register method")
        Log.d(TAG, "$name, $email, $phone, $password, $retryPassword")
        showProgressBar(true)
        val mergedPhone = "+62$phone"
        val registerRequest = RegisterRequest(name, email, mergedPhone, password, retryPassword)
        val userRegister = User(name=name, email = email, telephone = mergedPhone)
        viewModel.register(registerRequest, object : MyWalletRepository.RegisterCallback {
            override fun onSuccess(user: User?, message: String) {
                userRegister.idUser = user!!.idUser
                val intent = Intent(
                    this@LoginRegisterActivity, VerificationCodeActivity::class.java
                )
                intent.putExtra(VerificationCodeActivity.USER_DATA_EXTRA, userRegister)
                startActivity(intent)
                finish()
                toast(this@LoginRegisterActivity, message)
                showProgressBar(false)

            }

            override fun onFailure(message: String) {
                toast(this@LoginRegisterActivity, message)
                showProgressBar(false)
            }
        })
    }



    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            layout_progress_bar.visibility = View.VISIBLE
        } else {
            layout_progress_bar.visibility = View.GONE
        }
    }

    private fun saveUserPreference(user: User?) {
        val editor = sharedPreferences.edit()
        editor.putInt(Constants.PREF_KEY_USER_ID, user!!.idUser)
        editor.putString(Constants.PREF_KEY_USER_NAME, user.name)
        editor.putString(Constants.PREF_KEY_USER_EMAIL, user.email)
        editor.putString(Constants.PREF_KEY_USER_PHONE, user.telephone)
        editor.putString(Constants.PREF_KEY_PHOTO_URL, user.photoProfile)
        editor.putFloat(Constants.PREF_KEY_USER_BALANCE, user.balance.toFloat())
        editor.putString(Constants.PREF_KEY_TOKEN, user.token)
        editor.putBoolean(PREF_KEY_IS_LOGIN, true)
        editor.apply()
        editor.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                RESEND_OTP_REQUEST -> {
                    val user = User(
                        telephone = data?.getStringExtra(PHONE_NUMBER_EXTRA)!!.toString()
                    )
                    val intent = Intent(this, VerificationCodeActivity::class.java)
                    intent.putExtra(VerificationCodeActivity.USER_DATA_EXTRA, user)
                    intent.putExtra(VerificationCodeActivity.IS_RESEND_OTP_EXTRA, true)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }
}
