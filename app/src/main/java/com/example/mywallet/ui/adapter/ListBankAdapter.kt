package com.example.mywallet.ui.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.mywallet.ui.activity.BankPaymentActivity
import com.example.mywallet.ui.activity.PaymentActivity
import com.example.mywallet.R


class ListBankAdapter : RecyclerView.Adapter<ListBankAdapter.ListBankHolder>() {
    private var listBank: List<com.example.mywallet.db.entity.Bank> = ArrayList()!!
    val bundle = Bundle()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListBankHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bank, parent, false)
        return ListBankHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listBank.size
    }

    override fun onBindViewHolder(holder: ListBankHolder, position: Int) {
        val currentBank = listBank[position]
        holder.textViewBank.text = currentBank.bankName
        holder.cardBank.setOnClickListener {
            val activity = it.getContext() as PaymentActivity
            val preferences = activity.getSharedPreferences("PreferenceApp", 0)
            val editor = preferences.edit()
            editor.putInt("idBank", currentBank.idBank)
            editor.putString("nameBank", currentBank.bankName)
            editor.apply()
            val intent = Intent(activity, BankPaymentActivity::class.java)
            activity.startActivity(intent)
        }
    }

    inner class ListBankHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewBank: TextView = itemView.findViewById(R.id.textview_bank)
        var cardBank: CardView = itemView.findViewById(R.id.card_bank)
    }

    fun setBank(listBank: List<com.example.mywallet.db.entity.Bank>) {
        this.listBank = listBank
        notifyDataSetChanged()
    }
}