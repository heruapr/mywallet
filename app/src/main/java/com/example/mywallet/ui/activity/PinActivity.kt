package com.example.mywallet.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.mywallet.R
import com.example.mywallet.model.CheckPinRequest
import com.example.mywallet.model.UpdatePINRequest
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants.PHONE_REGEX
import com.example.mywallet.util.Constants.PREF_KEY_TOKEN
import com.example.mywallet.util.Constants.PREF_KEY_USER_ID
import com.example.mywallet.util.Constants.PREF_NAME
import com.example.mywallet.util.Constants.TAG
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_pin.*
import org.koin.android.ext.android.inject

class PinActivity : AppCompatActivity() {

    companion object {
        const val TYPE_EXTRA = "type_extra"
        const val USER_DATA_EXTRA = "user_data_extra"
    }

    private val viewModel: AuthViewModel by inject()
    private lateinit var sharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin)

        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        if (intent.getStringExtra(TYPE_EXTRA) != null) {
            when {
                intent.getStringExtra(TYPE_EXTRA) == "create" -> {
                    setupCreatePinUI(false)
                }
                intent.getStringExtra(TYPE_EXTRA) == "verification" -> {
                    setupVerificationUI()
                }
                intent.getStringExtra(TYPE_EXTRA) == "edit" -> {
                    setupCreatePinUI(true)
                }
                intent.getStringExtra(TYPE_EXTRA) == "otp" -> {
                    setupSendOtpUI()
                }
            }

        }

    }

    private fun setupCreatePinUI(isEdit: Boolean) {
        text_view_title.text = getString(R.string.buat_pin)
        text_view_subtitle.text = getString(R.string.buat_pin_detail)
        edit_text_pin.setOtpCompletionListener { pin ->
            edit_text_pin_confirmation.visibility = View.VISIBLE
            text_view_confirmation.visibility = View.VISIBLE

            edit_text_pin_confirmation.setOtpCompletionListener { retryPin ->
                if (retryPin != pin) {
                    toast(this, "PIN tidak sesuai!")
                } else {
//                    startActivity(Intent(this@PinActivity, MainActivity::class.java))
                    button_save_pin.visibility = View.VISIBLE
                    button_save_pin.setOnClickListener {
                        updatePin(pin, retryPin, isEdit)
                    }

                }
            }
        }
    }

    private fun setupVerificationUI() {
        text_view_title.text = getString(R.string.pin)
        text_view_subtitle.text = getString(R.string.masukkan_pin)

        edit_text_pin.setOtpCompletionListener { pin ->
            checkPin(pin)
        }
    }

    private fun setupSendOtpUI(){
        text_view_title.text = getString(R.string.nomor_telepon)
        text_view_subtitle.text = getString(R.string.masukkan_no_telp_terdaftar)
        edit_text_pin.visibility = View.GONE
        layout_phone_otp.visibility = View.VISIBLE
        button_send_otp.visibility = View.VISIBLE
        button_send_otp.setOnClickListener {
            if (isPhoneValid()){
                val phoneNo = "+62${edit_text_phone_otp.text.toString()}"
                val data = Intent()
                data.putExtra(LoginRegisterActivity.PHONE_NUMBER_EXTRA, phoneNo)
                setResult(Activity.RESULT_OK, data)
                finish()
//                resendOtp("+62${edit_text_phone_otp.text.toString()}")
                Log.d(TAG, "phone = ${edit_text_phone_otp.text.toString()}")
            }
        }

    }

    private fun isPhoneValid() : Boolean{
        if (edit_text_phone_otp.text.toString().isNotEmpty()){
            if (!edit_text_phone_otp.text.toString().matches(PHONE_REGEX)){
                text_input_layout_phone.error = getString(R.string.error_phone_not_valid)
                return false
            }
        }
        else{
            edit_text_phone_otp.error = getString(R.string.error_field_cannot_empty, "Nomor telepon")
            return false
        }
        return true
    }

//    private fun resendOtpNewNumber(phone: String){
//        showProgressBar(true)
//        val resendOTPRequest = ResendOTPRequest(phone)
//        viewModel.resendOTPNewNumber(resendOTPRequest, object : MyWalletRepository.ResendOTPCallback{
//            override fun onSuccess(idUser: Int, message: String) {
//                toast(this@PinActivity, message)
//                val data = Intent()
//                data.putExtra(LoginRegisterActivity.PHONE_NUMBER_EXTRA, phone)
//                data.putExtra(LoginRegisterActivity.USER_ID_EXTRA, idUser)
//                setResult(Activity.RESULT_OK, data)
//                showProgressBar(false)
//                finish()
//            }
//
//            override fun onFailure(message: String) {
//                toast(this@PinActivity, message)
//                showProgressBar(false)
//            }
//        })
//    }

    private fun checkPin(pin: String){
        showProgressBar(true)
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")
        val checkPinRequest = CheckPinRequest(pin.toInt())

        viewModel.checkPin(checkPinRequest, idUser, token.toString(), object : MyWalletRepository.CheckPinCallback{
            override fun onSuccess(message: String) {
                toast(this@PinActivity, message)
                setResult(Activity.RESULT_OK)
                finish()
                showProgressBar(false)
            }

            override fun onFailure(message: String) {
                toast(this@PinActivity, message)
                showProgressBar(false)

            }
        })
    }

    private fun updatePin(pin: String, retryPin: String, isEdit: Boolean) {
        showProgressBar(true)
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")
        val updatePINRequest = UpdatePINRequest(pin.toInt(), retryPin.toInt())

        viewModel.updatePin(
            updatePINRequest,
            idUser,
            token.toString(),
            object : MyWalletRepository.UpdatePINCallback {
                override fun onSuccess(message: String) {
                    if (isEdit){
                        toast(this@PinActivity, message)
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                    else{
                        startActivity(Intent(this@PinActivity, MainActivity::class.java))
                        toast(this@PinActivity, message)
                        finish()
                    }
                    showProgressBar(false)

                }

                override fun onFailure(message: String) {
                    toast(this@PinActivity, message)
                    showProgressBar(false)

                }
            })
    }

    private fun showProgressBar(isLoading: Boolean){
        if (isLoading){
            layout_progress_bar.visibility = View.VISIBLE
        }
        else{
            layout_progress_bar.visibility = View.GONE
        }
    }
}
