package com.example.mywallet.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.mywallet.R
import com.example.mywallet.model.UpdateUserDataRequest
import com.example.mywallet.model.User
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants.PHONE_REGEX
import com.example.mywallet.util.Constants.PREF_KEY_TOKEN
import com.example.mywallet.util.Constants.PREF_KEY_USER_ID
import com.example.mywallet.util.Constants.PREF_KEY_USER_PHONE
import com.example.mywallet.util.Constants.PREF_NAME
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.AuthViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_edit_phone.*
import org.koin.android.ext.android.inject

class EditPhoneActivity : AppCompatActivity() {

    companion object {
        private const val OTP_REQUEST = 99
    }

    private val viewModel: AuthViewModel by inject()
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var user: User


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_phone)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        setupUI()
    }

    private fun setupUI() {
        val phone = sharedPreferences.getString(PREF_KEY_USER_PHONE, "")?.substringAfter("+62")
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")
        edit_text_phone_edit.setText(phone)
        button_save_phone.setOnClickListener {
            if (isAllFieldValid()) {
                val newPhone = "+62${edit_text_phone_edit.text.toString()}"
                if (newPhone.substringAfter("+62") != phone) {
                    user = User(
                        idUser = idUser,
                        token = token,
                        telephone = newPhone
                    )
                    updatePhone()
                    val intent = Intent(this, VerificationCodeActivity::class.java)
                    intent.putExtra(VerificationCodeActivity.USER_DATA_EXTRA, user)
                    intent.putExtra(VerificationCodeActivity.IS_EDIT_PHONE_EXTRA, true)
                    startActivityForResult(intent, OTP_REQUEST)
                } else {
                    finish()
                }

            }
        }

        edit_text_phone_edit.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if (isFieldValid(edit_text_phone_edit, text_input_layout_phone, "Nomor telepon")){
                    clearError(text_input_layout_phone)
                    showValidSign(text_input_layout_phone)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun updatePhone() {
        layout_progress_bar.visibility = View.VISIBLE
        val idUser = sharedPreferences.getInt(PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(PREF_KEY_TOKEN, "")

        val updateUserDataRequest = UpdateUserDataRequest(
            user.name,
            user.email,
            user.telephone,
            user.photoProfile,
            user.password,
            user.newPassword,
            user.retryPassword
        )

        viewModel.updateUserData(
            updateUserDataRequest,
            idUser,
            token.toString(),
            object : MyWalletRepository.UpdateProfileCallback {
                override fun onSuccess(user: User?, message: String) {
                    toast(this@EditPhoneActivity, message)
                    layout_progress_bar.visibility = View.GONE
                }

                override fun onFailure(message: String) {
                    toast(this@EditPhoneActivity, message)
                    layout_progress_bar.visibility = View.GONE
                }
            })
    }

    private fun updateUserPrefData(user: User?) {
        val editor = sharedPreferences.edit()
        editor.putString(PREF_KEY_USER_PHONE, user?.telephone)
        editor.apply()
        editor.commit()
    }

    private fun isFieldValid(
        editText: TextInputEditText,
        textInputLayout: TextInputLayout,
        name: String
    ): Boolean {
        if (editText.text.toString().isNotEmpty()) {
            if (!editText.text.toString().matches(PHONE_REGEX)) {
                setFieldError(textInputLayout, getString(R.string.error_phone_not_valid))
                return false
            }

        } else {
            setFieldError(textInputLayout, getString(R.string.error_field_cannot_empty, name))
            return false
        }

        return true

    }

    private fun setFieldError(textInputLayout: TextInputLayout, message: String) {
        textInputLayout.error = message
    }

    private fun clearError(textInputLayout: TextInputLayout) {
        textInputLayout.error = null
    }

    private fun showValidSign(textInputLayout: TextInputLayout) {
        textInputLayout.boxStrokeColor = resources.getColor(R.color.colorGreenSuccess)
    }

    private fun isAllFieldValid(): Boolean {
        isFieldValid(edit_text_phone_edit, text_input_layout_phone, "Nomor telepon")

        return (text_input_layout_phone.error.isNullOrEmpty())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }

        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                OTP_REQUEST -> {
//                    updatePhone()
                    updateUserPrefData(user)
                    finish()
                }
            }
        }
    }
}

