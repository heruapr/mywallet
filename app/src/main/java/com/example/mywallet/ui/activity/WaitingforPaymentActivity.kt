package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.mywallet.R
import com.example.mywallet.model.Transaction
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants
import com.example.mywallet.viewmodel.TransactionViewModel
import kotlinx.android.synthetic.main.activity_waitingfor_payment.*
import org.koin.android.ext.android.inject

class WaitingforPaymentActivity : AppCompatActivity() {
    var idBank: Int = 0
    var payAmount: String = ""
    var taxNumber: String = ""
    var year: String = ""
    var idTrasaction: Int = 0
    var vaNumber: String = ""
    private val viewModel: TransactionViewModel by inject()

    var token: String = ""
    var idUser: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waitingfor_payment)
        val preferences = getSharedPreferences("PreferenceApp", 0)
        val sharedPreferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val nameBank = preferences.getString("nameBank", "")

        //idBank = preferences.getInt("idbank", 0)

        if (intent.extras == null) {
            taxNumber = preferences.getString(Constants.PREF_KEY_TAX_NUMBER, "").toString()
            payAmount = preferences.getString(Constants.PREF_KEY_TAX_PAY_AMOUNT, "").toString()
            vaNumber = preferences.getString(Constants.PREF_KEY_TAX_VA_NUMBER, "").toString()
        } else {
            taxNumber = intent.getStringExtra(Constants.BUNDLE_KEY_TAX_BILL)
            payAmount = intent.getStringExtra(Constants.BUNDLE_KEY_TAX_BILL)
            vaNumber  = intent.getStringExtra(Constants.BUNDLE_KEY_TAX_VA_NUMBER)
        }

        textview_bank.setText(nameBank.toString())
        textview_bill.setText(payAmount)
        textview_vaNumber.setText(vaNumber)

        textview_total.setText(payAmount)

        button_confirm_payment.isClickable = false
        button_confirm_payment.isEnabled = false
        button_confirm_payment.setBackgroundColor(R.drawable.button_cek_tagihan_background_inactive)

        token = sharedPreferences.getString(Constants.PREF_KEY_TOKEN, "").toString()
        idUser = sharedPreferences.getInt(Constants.PREF_KEY_USER_ID, 0)

        refreshPage()

        button_confirm_payment.setOnClickListener {
            startActivity(Intent(this, PaymentSuccessActivity::class.java))
            finish()
        }
    }

    fun getSingleTransaction(idUser: Int, idTransaction: Int, token: String) {
        viewModel.getSingleTransaction(
            idUser, idTransaction,
            token,
            object : MyWalletRepository.GetSingleTransactionCallback {
                override fun onSuccess(transaction: Transaction, message: String) {
                    if (transaction.isFinished == 1) {
                        button_confirm_payment.isClickable = true
                        button_confirm_payment.isEnabled = true
                        button_confirm_payment.setBackgroundColor(R.drawable.button_cek_tagihan_background_primary)
                        Log.d("PAYMENT BANK : ", "SUCESSS!")
//                        startActivity(
//                            Intent(
//                                this@WaitingforPaymentActivity,
//                                PaymentSuccessActivity::class.java
//                            )
//                        )
                        finish()
                    } else {
                        Toast.makeText(
                            this@WaitingforPaymentActivity,
                            "please finish the payment",
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d("PAYMENT BANK : ", "please finish the payment")
                    }
                }

                override fun onFailure(message: String) {
                    Log.d("PAYMENT BANK : ", "FAILED BECAUSE OF : " + message)
                }
            })
    }


    fun refreshPage() {
        val preferences = getSharedPreferences("PreferenceApp", 0)
        idTrasaction = preferences.getInt(Constants.PREF_KEY_TAX_ID, 0)
        swiperefresh.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            getSingleTransaction(idUser, idTrasaction, token)
            // Your code to make your refresh action
            // CallYourRefreshingMethod();
            val handler = Handler()
            handler.postDelayed({
                if (swiperefresh.isRefreshing()) {
                    swiperefresh.setRefreshing(false)
                }
            }, 1000)
            // Your code to make your refresh action
            // CallYourRefreshingMethod();
        })
    }
}

