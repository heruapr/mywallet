package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mywallet.R
import com.example.mywallet.util.Constants
import kotlinx.android.synthetic.main.activity_wallet_payment.*
import kotlinx.android.synthetic.main.activity_wallet_payment.textview_total

class WalletPaymentActivity : AppCompatActivity() {
    var payAmount: String = ""
    var taxNumber: String = ""
    var year: String = ""
    val bundle = Bundle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet_payment)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val preferences = getSharedPreferences("PreferenceApp", 0)
        val sharedPreferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
        textview_balance.setText(
            sharedPreferences.getFloat(
                Constants.PREF_KEY_USER_BALANCE,
                0f
            ).toString()
        )
        taxNumber = preferences.getString(Constants.PREF_KEY_TAX_NUMBER, "").toString()
        payAmount = preferences.getString(Constants.PREF_KEY_TAX_PAY_AMOUNT, "").toString()
        year = preferences.getString(Constants.PREF_KEY_TAX_YEAR, "").toString()
        textview_bill.setText(payAmount)
        textview_total.setText(payAmount)

        button_confirm.setOnClickListener {
            val intent = Intent(this, PaymentPinActivity::class.java)
            startActivity(intent)
        }
    }

}
