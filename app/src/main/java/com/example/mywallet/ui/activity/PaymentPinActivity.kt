package com.example.mywallet.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.mywallet.R
import com.example.mywallet.model.CheckPinRequest
import com.example.mywallet.model.eWalletPayment
import com.example.mywallet.model.eWalletPaymentRequest
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants
import com.example.mywallet.viewmodel.AuthViewModel
import com.example.mywallet.viewmodel.TransactionViewModel
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_payment_pin.edit_text_pin
import kotlinx.android.synthetic.main.activity_payment_pin.text_view_subtitle
import kotlinx.android.synthetic.main.activity_payment_pin.text_view_title
import kotlinx.android.synthetic.main.activity_payment_pin.layout_progress_bar
import org.koin.android.ext.android.inject

class PaymentPinActivity : AppCompatActivity() {

    private val viewModelAuth: AuthViewModel by inject()
    private val viewModel: TransactionViewModel by inject()

    var payAmount: String = ""
    var taxNumber: String = ""
    var year: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_pin)
        val preferences = getSharedPreferences("PreferenceApp", 0)
        taxNumber = preferences.getString(Constants.PREF_KEY_TAX_NUMBER, "").toString()
        payAmount = preferences.getString(Constants.PREF_KEY_TAX_PAY_AMOUNT, "").toString()
        year = preferences.getString(Constants.PREF_KEY_TAX_YEAR, "").toString()
        setupVerificationUI()
    }

    fun getEwalletPayment(payAmount: Double, taxNumber: String, year: Int) {
        val intent = Intent(this, PaymentSuccessActivity::class.java)
        val preferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
        val eWalletPayment = eWalletPaymentRequest(taxNumber, payAmount, year, "pbb")
        val userId = preferences.getInt(Constants.PREF_KEY_USER_ID, 0)
        val token = preferences.getString(Constants.PREF_KEY_TOKEN, "")
        viewModel.getWalletPayment(
            userId, token.toString(),
            eWalletPayment,
            object : MyWalletRepository.getWalletPaymentCallback {
                override fun onSuccess(message: String) {
                    Toast.makeText(this@PaymentPinActivity, message, Toast.LENGTH_SHORT)
                        .show()
                    Log.d("enterpayment EWALLET : ", "SUCESSS!")
                    startActivity(intent)
                    finish()
                }
                override fun onFailure(message: String) {
                    Log.d("enterpayment EWALLET : ", "FAILED BECAUSE OF : " + message)
                    Toast.makeText(this@PaymentPinActivity, "PAYMENT FAILED", Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    fun validatePayment(pin: String) {
        showProgressBar(true)
        val preferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
        val idUser = preferences.getInt(Constants.PREF_KEY_USER_ID, 0)
        val token = preferences.getString(Constants.PREF_KEY_TOKEN, "")
        val checkPinRequest = CheckPinRequest(pin.toInt())

        viewModelAuth.checkPin(
            checkPinRequest,
            idUser,
            token.toString(),
            object : MyWalletRepository.CheckPinCallback {
                override fun onSuccess(message: String) {
                    Constants.toast(this@PaymentPinActivity, message)
                    setResult(Activity.RESULT_OK)
                    getEwalletPayment(payAmount.toDouble(), taxNumber, year.toInt())
                    showProgressBar(false)
                }
                override fun onFailure(message: String) {
                    Constants.toast(this@PaymentPinActivity, message)
                    showProgressBar(false)
                }
            })
    }

    private fun setupVerificationUI() {
        text_view_title.text = getString(R.string.pin)
        text_view_subtitle.text = getString(R.string.masukkan_pin)

        edit_text_pin.setOtpCompletionListener { pin ->
            validatePayment(pin)
        }
    }

    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            layout_progress_bar.visibility = View.VISIBLE
        } else {
            layout_progress_bar.visibility = View.GONE
        }
    }
}
