package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.mywallet.R
import com.example.mywallet.util.Constants.PREF_KEY_IS_LOGIN
import com.example.mywallet.util.Constants.PREF_NAME


class SplashActivity : AppCompatActivity() {

    private lateinit var sharedPreferences: SharedPreferences

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        window.statusBarColor = Color.TRANSPARENT

        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val isLoggedIn = sharedPreferences.getBoolean(PREF_KEY_IS_LOGIN, false)

        Handler().postDelayed({
            if (isLoggedIn) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                startActivity(Intent(this, LoginRegisterActivity::class.java))

            }
            finish()
        }, 3000L)
    }
}
