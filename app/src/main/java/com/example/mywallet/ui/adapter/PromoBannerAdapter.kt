package com.example.mywallet.ui.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mywallet.R

class PromoBannerAdapter : RecyclerView.Adapter<PromoBannerAdapter.ViewHolder>() {
    private lateinit var list : ArrayList<Drawable>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_banner, parent, false)
        return ViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]

        Glide.with(holder.itemView.context).load(item).into(holder.imageViewBanner)
    }
    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: ArrayList<Drawable>){
        this.list = list
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageViewBanner: ImageView = itemView.findViewById(R.id.image_view_banner)
    }
}