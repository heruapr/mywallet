package com.example.mywallet.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.mywallet.R
import com.example.mywallet.model.BankPayment
import com.example.mywallet.model.BankPaymentRequest
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants
import com.example.mywallet.viewmodel.TransactionViewModel
import kotlinx.android.synthetic.main.activity_bank_payment.*
import kotlinx.android.synthetic.main.activity_bank_payment.progress_bar
import org.koin.android.ext.android.inject


class BankPaymentActivity : AppCompatActivity() {
    var idBank: Int = 0
    var payAmount: String = ""
    var taxNumber: String = ""
    var year: String = ""
    private val viewModel: TransactionViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_payment)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        showProgressBar(false)
        val preferences = getSharedPreferences("PreferenceApp", 0)
        val nameBank = preferences.getString("nameBank", "")
        textview_bank.setText(nameBank.toString())
        taxNumber = preferences.getString(Constants.PREF_KEY_TAX_NUMBER, "").toString()
        payAmount = preferences.getString(Constants.PREF_KEY_TAX_PAY_AMOUNT, "").toString()
        year = preferences.getString(Constants.PREF_KEY_TAX_YEAR, "").toString()
        idBank = preferences.getInt("idBank", 0)
        textview_bill.setText(payAmount)
        textview_total.setText(payAmount)

        button_confirm.setOnClickListener {
            getBankPayment(payAmount.toDouble(), taxNumber, year.toInt())
        }
    }

    fun getBankPayment(amount: Double, taxNumber: String, year: Int) {
        showProgressBar(true)
        val intent = Intent(this, WaitingforPaymentActivity::class.java)
        val preferences = getSharedPreferences("PreferenceApp", 0)
        val editor = preferences.edit()
        val sharedPreferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
        val bankPayment = BankPaymentRequest(taxNumber, amount, year, "pbb", idBank)
        val userId = sharedPreferences.getInt(Constants.PREF_KEY_USER_ID, 0)
        val token = sharedPreferences.getString(Constants.PREF_KEY_TOKEN, "")
        viewModel.getBankPayment(
            userId, token.toString(),
            bankPayment,
            object : MyWalletRepository.getBankPaymentCallback {
                override fun onSuccess(bank: BankPayment, message: String) {
//                    Toast.makeText(this, "PAYMENT SUCCESS", Toast.LENGTH_SHORT).show()
                    Log.d("create payment bank: ", "SUCESSS!")
                    editor.putInt(Constants.PREF_KEY_TAX_ID, bank.idTransaction)
                    editor.putString(Constants.PREF_KEY_TAX_VA_NUMBER, bank.virtualNumber)
                    editor.apply()
                    showProgressBar(false)
                    startActivity(intent)
                    finish()
                }
                override fun onFailure(message: String) {
                    Log.d("enterpayment bank : ", "FAILED BECAUSE OF : " + message)
                }
            })
    }

    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            progress_bar.visibility = View.VISIBLE
        } else {
            progress_bar.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, PBBActivity::class.java))
        finish()
    }
}