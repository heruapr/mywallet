package com.example.mywallet.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.mywallet.R
import com.example.mywallet.model.OTPValidationRequest
import com.example.mywallet.model.ResendOTPRequest
import com.example.mywallet.model.UpdateUserDataRequest
import com.example.mywallet.model.User
import com.example.mywallet.repository.MyWalletRepository
import com.example.mywallet.util.Constants.TAG
import com.example.mywallet.util.Constants.toast
import com.example.mywallet.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_verification_code.*
import org.koin.android.ext.android.inject

class VerificationCodeActivity : AppCompatActivity() {

    companion object {
        const val USER_DATA_EXTRA = "user_data_extra"
        const val IS_RESEND_OTP_EXTRA = "is_resend_otp_extra"
        const val IS_EDIT_PHONE_EXTRA = "is_edit_phone_extra"
    }

    private val viewModel: AuthViewModel by inject()
    private lateinit var user: User
    private var isEditPhone = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification_code)
        if (intent.getParcelableExtra<User>(USER_DATA_EXTRA) != null) {
            user = intent.getParcelableExtra(USER_DATA_EXTRA) as User
            Log.d(TAG, "User verif = $user")
        }

        if (intent.getBooleanExtra(IS_RESEND_OTP_EXTRA, false)) {
            resendOtp()
        }

        if (intent.getBooleanExtra(IS_EDIT_PHONE_EXTRA, false)) {
            isEditPhone = true
//            resendOtp()
        }

        setupUI()

    }

    private fun setupUI() {
        text_view_subtitle.text = getString(R.string.verification_code_message, user.telephone)
        edit_text_otp.setOtpCompletionListener {
            //            val intent = Intent(this@VerificationCodeActivity, PinActivity::class.java)
//            intent.putExtra(PinActivity.TYPE_EXTRA, "create")
//            intent.putExtra(PinActivity.USER_DATA_EXTRA, user)
//            startActivity(intent)
            if (isEditPhone) {
                validateOtpNewNumber(it)
            } else {
                validateOtp(it)
            }
        }
        text_view_resend_otp.setOnClickListener {
            resendOtp()
        }
    }

    private fun validateOtp(code: String) {
        Log.d(TAG, "OTP = $code")
        showProgressBar(true)
        val otpValidationRequest = OTPValidationRequest(user.idUser, code)
        viewModel.otpValidation(
            otpValidationRequest,
            object : MyWalletRepository.OTPValidationCallback {
                override fun onSuccess(message: String) {
                    val intent =
                        Intent(this@VerificationCodeActivity, LoginRegisterActivity::class.java)
                    startActivity(intent)
                    finish()

                    toast(this@VerificationCodeActivity, message)
                    showProgressBar(false)
                }

                override fun onFailure(message: String) {
                    toast(this@VerificationCodeActivity, message)
                    showProgressBar(false)
                }
            })
    }

    private fun validateOtpNewNumber(code: String) {
        Log.d(TAG, "OTP New= $code ID = ${user.idUser} Token = ${user.token}")
        showProgressBar(true)
        val otpValidationRequest = OTPValidationRequest(user.idUser, code)
        viewModel.otpValidationNewNumber(
            otpValidationRequest, user.idUser, user.token.toString(),
            object : MyWalletRepository.OTPValidationCallback {
                override fun onSuccess(message: String) {
                    setResult(Activity.RESULT_OK)
                    finish()

                    toast(this@VerificationCodeActivity, message)
                    showProgressBar(false)
                }

                override fun onFailure(message: String) {
                    toast(this@VerificationCodeActivity, message)
                    showProgressBar(false)
                }
            })
    }


    private fun resendOtp() {
        showProgressBar(true)
        if (isEditPhone) {
            val updateUserDataRequest = UpdateUserDataRequest(
                user.name,
                user.email,
                user.telephone,
                user.photoProfile,
                user.password,
                user.newPassword,
                user.retryPassword
            )

            viewModel.updateUserData(
                updateUserDataRequest,
                user.idUser,
                user.token.toString(),
                object : MyWalletRepository.UpdateProfileCallback {
                    override fun onSuccess(user: User?, message: String) {
                        toast(this@VerificationCodeActivity, message)
                        showProgressBar(false)
                    }

                    override fun onFailure(message: String) {
                        toast(this@VerificationCodeActivity, message)
                        showProgressBar(false)
                    }
                })
        } else {
            val resendOTPRequest = ResendOTPRequest(user.telephone.toString())
            viewModel.resendOTP(resendOTPRequest, object : MyWalletRepository.ResendOTPCallback {
                override fun onSuccess(idUser: Int, message: String) {
                    user.idUser = idUser
                    toast(this@VerificationCodeActivity, message)
                    showProgressBar(false)
                }

                override fun onFailure(message: String) {
                    toast(this@VerificationCodeActivity, message)
                    showProgressBar(false)
                }
            })

        }

    }


    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            layout_progress_bar.visibility = View.VISIBLE
        } else {
            layout_progress_bar.visibility = View.GONE
        }
    }
}
