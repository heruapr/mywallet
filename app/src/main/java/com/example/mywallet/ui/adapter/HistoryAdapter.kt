package com.example.mywallet.ui.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.mywallet.R
import com.example.mywallet.model.Transaction
import kotlin.math.roundToInt

class HistoryAdapter : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {
    private lateinit var list: ArrayList<Transaction>
    private lateinit var listener:  HistoryListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_history, parent, false)
        return ViewHolder(itemView)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.textViewService.text = item.service
        holder.textViewTrxId.text = "ID : ${item.idTransaction}"
        holder.textViewTrxDate.text = item.dateTransaction
        holder.textViewTrxAmount.text = "Rp. ${item.amount.roundToInt()}"

        if (item.isFinished == 1) {
            holder.textViewTrxStatus.text = holder.itemView.context.getString(R.string.sukses)
            holder.textViewTrxStatus.setBackgroundResource(R.drawable.rounded_background_green)
        } else {
            holder.textViewTrxStatus.text = holder.itemView.context.getString(R.string.pending)
            holder.textViewTrxStatus.setBackgroundResource(R.drawable.rounded_background_red)
            holder.itemView.setOnClickListener {
                listener.onItemClicked(item)
            }
        }

        if (item.isUseWallet == 1) {
            holder.textViewTrxAmount.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                holder.itemView.context.getDrawable(R.drawable.logo_dana_24),
                null
            )
        } else {
            holder.textViewTrxAmount.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                holder.itemView.context.getDrawable(R.drawable.ic_bank_card_blue_24),
                null
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: ArrayList<Transaction>) {
        this.list = list
        notifyDataSetChanged()
    }

    fun setListener(listener: HistoryListener){
        this.listener = listener
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewService: TextView = itemView.findViewById(R.id.text_view_service)
        val textViewTrxId: TextView = itemView.findViewById(R.id.text_view_trx_id)
        val textViewTrxDate: TextView = itemView.findViewById(R.id.text_view_trx_date)
        val textViewTrxAmount: TextView = itemView.findViewById(R.id.text_view_trx_amount)
        val textViewTrxStatus: TextView = itemView.findViewById(R.id.text_view_trx_status)
    }

    interface HistoryListener{
        fun onItemClicked(transaction: Transaction)
    }
}