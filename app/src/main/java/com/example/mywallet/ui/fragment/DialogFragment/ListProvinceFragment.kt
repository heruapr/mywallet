package com.example.mywallet.ui.fragment.DialogFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mywallet.R
import com.example.mywallet.ui.adapter.ListProvinceAdapter
import com.example.mywallet.db.entity.Province
import com.example.mywallet.rest.ApiClient
import com.example.mywallet.util.Constants
import com.example.mywallet.util.Constants.TAG
import com.example.mywallet.viewmodel.ProvinceViewModel
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_list_province.*
import kotlinx.android.synthetic.main.fragment_year_pbb.button_close
import org.json.JSONObject
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class ListProvinceFragment : Fragment() {
    private val adapter: ListProvinceAdapter by inject()
    private val provinceViewModel: ProvinceViewModel by inject()
    var update: String = "update"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_province, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        province_recyclerview.layoutManager = LinearLayoutManager(this.requireContext())
        province_recyclerview.setHasFixedSize(true)
        province_recyclerview.adapter = adapter

        setupRecyclerView()

        getProvince()

        button_close.setOnClickListener {
            requireActivity().onBackPressed()
        }
        edit_text_search_province.setOnClickListener {
        }
    }

    private fun setupRecyclerView() {
        showProgressBar(false)
        provinceViewModel.getAllProvinces().observe(this,
            Observer<List<Province>> { list ->
                list?.let {
                    adapter.setProvince(it)
                }
            })
        Log.d("SAMPAI OBSERVER", "sampai OBSERVER")
    }


    fun getProvince() {
        ApiClient().services.getProvince()
            .enqueue(object : Callback<JsonObject> {
                override fun onFailure(
                    call: Call<JsonObject>,
                    t: Throwable
                ) {
                    Log.d(Constants.TAG, "error getProvince 1 : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {
                    if (response.isSuccessful) {
                        val body = response.body()
                        Log.d(Constants.TAG, "MANTAP province : $body")
                        if(JSONObject(body.toString()).getInt("status").equals(200)) {
                            writeData(body.toString())
                        }else{
                            Log.d(TAG,"error server")
                        }
                    } else {
                        Log.d(Constants.TAG, "getting province error :")
                    }
                }
            })
    }

    fun writeData(response: String) {
        showProgressBar(true)
        val obj = JSONObject(response).getJSONObject("data")
        val dataArray = obj.getJSONArray("provinces")
        Log.d("WRITE DATA", "sampai WRITE , lengt array obj: ${dataArray.length()}")
        for (i in 0..dataArray.length() - 1) {
            val dataobj = dataArray.getJSONObject(i)
            //save to sqlite
            Log.d("DATA PROV 1", dataobj.getString("provinceName").toString())
            Log.d("DATA PROV 2", dataobj.getString("idProvince").toString())
            provinceViewModel.insert(
                Province(
                    dataobj.getString("idProvince").toInt(),
                    dataobj.getString("provinceName").toString()
                )
            )
            Log.d("SIMPAN", "SAVED DATA")
        }
        setupRecyclerView()
    }

    private fun showProgressBar(isLoading: Boolean) {
        if (isLoading) {
            progress_bar.visibility = View.VISIBLE
        } else {
            progress_bar.visibility = View.GONE
        }
    }
}

