package com.example.mywallet.viewmodel

import androidx.lifecycle.ViewModel
import com.example.mywallet.model.*
import com.example.mywallet.repository.MyWalletRepository
import okhttp3.MultipartBody


class AuthViewModel(private val repository: MyWalletRepository) : ViewModel() {

    fun login(loginRequest: LoginRequest, callback: MyWalletRepository.LoginCallback) {
        repository.login(loginRequest, callback)
    }

    fun register(registerRequest: RegisterRequest, callback: MyWalletRepository.RegisterCallback) {
        repository.register(registerRequest, callback)
    }

    fun otpValidation(
        otpValidationRequest: OTPValidationRequest,
        callback: MyWalletRepository.OTPValidationCallback
    ) {
        repository.otpValidation(otpValidationRequest, callback)
    }

    fun otpValidationNewNumber(
        otpValidationRequest: OTPValidationRequest, idUser: Int, token: String,
        callback: MyWalletRepository.OTPValidationCallback
    ) {
        repository.otpValidationNewNumber(otpValidationRequest, idUser, token, callback)
    }

    fun updatePin(
        updatePINRequest: UpdatePINRequest,
        idUser: Int,
        token: String,
        callback: MyWalletRepository.UpdatePINCallback
    ) {
        repository.updatePin(updatePINRequest, idUser, token, callback)
    }

    fun resendOTP(
        resendOTPRequest: ResendOTPRequest,
        callback: MyWalletRepository.ResendOTPCallback
    ) {
        repository.resendOtp(resendOTPRequest, callback)
    }

    fun logout(idUser: Int, token: String, callback: MyWalletRepository.LogoutCallback) {
        repository.logout(idUser, token, callback)
    }

    fun updateUserData(
        updateUserDataRequest: UpdateUserDataRequest,
        idUser: Int, token: String, callback: MyWalletRepository.UpdateProfileCallback
    ) {
        repository.updateUserData(updateUserDataRequest, idUser, token, callback)
    }

    fun uploadPhoto(filePart: MultipartBody.Part, idUser: Int, token: String, callback: MyWalletRepository.UploadPhotoCallback){
        repository.uploadPhoto(filePart, idUser, token, callback)
    }

    fun checkPin(
        checkPinRequest: CheckPinRequest,
        idUser: Int,
        token: String,
        callback: MyWalletRepository.CheckPinCallback
    ) {
        repository.checkPin(checkPinRequest, idUser, token, callback)
    }

    fun getUserData(userId: Int, token: String, callback: MyWalletRepository.GetUserDataCallback){
        repository.getUserData(userId, token, callback)
    }
}