package com.example.mywallet.viewmodel

import androidx.lifecycle.ViewModel
import com.example.mywallet.repository.MyWalletRepository

class MainViewModel(private val repository: MyWalletRepository) : ViewModel() {

    fun getUserData(userId: Int, token: String, callback: MyWalletRepository.GetUserDataCallback){
        repository.getUserData(userId, token, callback)
    }
}