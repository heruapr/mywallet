package com.example.mywallet.viewmodel

import androidx.lifecycle.ViewModel
import com.example.mywallet.repository.MyWalletRepository

class CityViewModel(private var repository: MyWalletRepository) : ViewModel() {

    fun getListCity(idProvince: Int, callback: MyWalletRepository.GetListCityCallback){
        repository.getListCity(idProvince, callback)
    }
}