package com.example.mywallet.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mywallet.db.entity.Province
import com.example.mywallet.repository.ProvinceRepository

class ProvinceViewModel(private var repository: ProvinceRepository) : ViewModel() {
    private var allProvinces: LiveData<List<Province>> = repository.getAllProvinces()
    fun insert(province: Province) {
        repository.insert(province)
    }
    fun deleteAllProvinces() {
        repository.deleteAllProvinces()
    }
    fun getAllProvinces(): LiveData<List<Province>> {
        return allProvinces
    }
}