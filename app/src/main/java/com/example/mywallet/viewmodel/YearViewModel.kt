package com.example.mywallet.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mywallet.db.entity.Year
import com.example.mywallet.repository.YearRepository

class YearViewModel(private var repository: YearRepository) : ViewModel() {
    private var allYears: LiveData<List<Year>> = repository.getAllYears()
    fun insert(year: Year) {
        repository.insert(year)
    }
    fun deleteAllYears() {
        repository.deleteAllYears()
    }
    fun getAllYears(): LiveData<List<Year>> {
        return allYears
    }
}