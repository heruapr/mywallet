package com.example.mywallet.viewmodel

import androidx.lifecycle.ViewModel
import com.example.mywallet.model.BankPaymentRequest
import com.example.mywallet.model.CheckBillRequest
import com.example.mywallet.model.eWalletPaymentRequest
import com.example.mywallet.repository.MyWalletRepository

class TransactionViewModel(private val repository: MyWalletRepository) : ViewModel() {

    fun checkBill(
        userId: Int,
        token: String,
        checkBillRequest: CheckBillRequest,
        callback: MyWalletRepository.getBillCallback
    ) {
        repository.getBill(userId, token, checkBillRequest, callback)
    }

    fun getWalletPayment(
        userId: Int,
        token: String,
        ewalletPaymentRequest: eWalletPaymentRequest,
        callback: MyWalletRepository.getWalletPaymentCallback
    ) {
        repository.getWalletPayment(userId, token, ewalletPaymentRequest, callback)
    }

    fun getBankPayment(
        userId: Int,
        token: String,
        bankPaymentRequest: BankPaymentRequest,
        callback: MyWalletRepository.getBankPaymentCallback
    ) {
        repository.getBankPayment(userId, token, bankPaymentRequest, callback)
    }

    fun getSingleTransaction(
        userId: Int,
        transactionId: Int,
        token: String,
        callback: MyWalletRepository.GetSingleTransactionCallback
    ) {
        repository.getSingleTransaction(userId, transactionId, token, callback)
    }
}