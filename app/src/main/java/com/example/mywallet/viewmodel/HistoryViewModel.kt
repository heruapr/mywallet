package com.example.mywallet.viewmodel

import androidx.lifecycle.ViewModel
import com.example.mywallet.repository.MyWalletRepository

class HistoryViewModel(private val repository: MyWalletRepository) : ViewModel(){
    fun getAllTransaction(userId: Int, token: String, callback: MyWalletRepository.GetAllTransactionCallback){
        repository.getAllTransaction(userId, token, callback)
    }
}