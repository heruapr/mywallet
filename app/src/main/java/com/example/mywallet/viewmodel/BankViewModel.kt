package com.example.mywallet.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mywallet.db.entity.Bank
import com.example.mywallet.repository.BankRepository

class BankViewModel(private var repository: BankRepository) : ViewModel() {
    private var allBanks: LiveData<List<Bank>> = repository.getAllBanks()
    fun insert(bank: Bank) {
        repository.insert(bank)
    }
    fun deleteAllBanks() {
        repository.deleteAllBanks()
    }
    fun getAllBanks(): LiveData<List<Bank>> {
        return allBanks
    }
}