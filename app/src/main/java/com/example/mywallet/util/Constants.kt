package com.example.mywallet.util

import android.content.Context
import android.widget.Toast

object Constants {
    val TAG = "MyWallet"

    //val BASE_URL = "https://a6f1c03d.ngrok.io"
    val BASE_URL = "http://ec2-3-89-102-145.compute-1.amazonaws.com:9705"
    val PREF_NAME = "my_wallet_pref"
    val EMAIL_REGEX = Regex("\\b[\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b")
    val PASSWORD_REGEX = Regex("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\W]).{8,})")
    val PHONE_REGEX = Regex("(^(^8)\\d{9,10})")
    val NAME_REGEX =
        Regex("^(?!.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?!.*[!@#\$%^&*])(?=\\S)(?=.*['\\-_]{0,}).{4,}\$")
    val API_INVALID_OTP_MESSAGE = "OTP is invalid or outdated."
    val API_RESEND_OTP_SUCCESS_MESSAGE = "OTP has been sent to your phone."
    val API_ACCOUNT_NOT_VERIFIED_MESSAGE = "This account is not verified yet."
    val API_LOGOUT_SUCCESS_MESSAGE = "You are logged-out."
    val PREF_KEY_USER_ID = "user_id"
    val PREF_KEY_USER_NAME = "user_name"
    val PREF_KEY_USER_EMAIL = "user_email"
    val PREF_KEY_USER_PHONE = "user_phone"
    val PREF_KEY_USER_BALANCE = "user_balance"
    val PREF_KEY_TOKEN = "token"
    val PREF_KEY_IS_LOGIN = "is_logged_in"


    val PREF_KEY_TAX_NUMBER = "tax_number"
    val PREF_KEY_TAX_PAY_AMOUNT = "tax_pay_amount"
    val PREF_KEY_TAX_ADDRESS = "tax_address"
    val PREF_KEY_TAX_GROUND_AREA = "tax_ground_area"
    val PREF_KEY_TAX_CITY_NAME = "tax_city_name"
    val PREF_KEY_TAX_YEAR = "tax_year"
    val PREF_KEY_TAX_LATITUDE = "tax_latitude"
    val PREF_KEY_TAX_LONGITUDE = "tax_longitude"
    val PREF_KEY_TAX_BUILDING_AREA = "tax_building_area"
    val PREF_KEY_TAX_PROVINCE_NAME = "tax_province_name"
    val PREF_KEY_TAX_VA_NUMBER = "tax_va_number"

    val PREF_KEY_TAX_ID = "tax_id"

    val BUNDLE_KEY_TAX_ID = "tax_id"
    val BUNDLE_KEY_TAX_BILL = "tax_bill"
    val BUNDLE_KEY_TAX_VA_NUMBER = "tax_VA_NUMBER"


    val PREF_KEY_PIN = "pin"
    val PREF_KEY_PHOTO_URL = "photo_url"

    fun toast(context: Context, message: String) =
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}