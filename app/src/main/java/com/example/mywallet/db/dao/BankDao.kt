package com.example.mywallet.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mywallet.db.entity.Bank

@Dao
interface BankDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(bank: Bank)

    @Query("DELETE FROM banks_table")
    fun deleteAllBanks()

    @Query("SELECT * FROM banks_table")
    fun getAllBanks(): LiveData<List<Bank>>
}