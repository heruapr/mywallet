package com.example.mywallet.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "provinces_table")

data class Province(
    var idProvince: Int,
    var provinceName: String
)
{
    @PrimaryKey()
    var id: Int = idProvince
}