package com.example.mywallet.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mywallet.db.entity.Year

@Dao
interface YearDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(year: Year)

    @Query("DELETE FROM years_table")
    fun deleteAllYears()

    @Query("SELECT * FROM years_table")
    fun getAllYears(): LiveData<List<Year>>
}