package com.example.mywallet.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "years_table")

data class Year(
    var year:Int
)
{
    @PrimaryKey()
    var id: Int = year
}