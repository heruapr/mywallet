package com.example.mywallet.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mywallet.db.entity.Province

@Dao
interface ProvinceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(province: Province)

    @Query("DELETE FROM provinces_table")
    fun deleteAllProvinces()

    @Query("SELECT * FROM provinces_table")
    fun getAllProvinces(): LiveData<List<Province>>
}