package com.example.mywallet.db

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.mywallet.db.dao.YearDao
import com.example.mywallet.db.entity.Year

@Database(entities = [Year::class], version = 1)

abstract class YearDatabase : RoomDatabase() {
    abstract fun yearDao(): YearDao

    companion object {

        private var instance: YearDatabase? = null
        fun getInstance(context: Context): YearDatabase {
            if (instance == null) {
                synchronized(YearDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        YearDatabase::class.java, "year_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build()
                }
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }

        private val roomCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                PopulateDbAsyncTask(instance)
                    .execute()
            }
        }

        class PopulateDbAsyncTask(db: YearDatabase?) : AsyncTask<Unit, Unit, Unit>() {
            private val yearDao = db?.yearDao()

            override fun doInBackground(vararg p0: Unit?) {
//                noteDao?.insert(
//                    Note(
//                        0,
//                        "Sport",
//                        "https://homepages.cae.wisc.edu/~ece533/images/peppers.png",
//                        "judul berita",
//                    "beritaaa nihhh"
//                    )
//                )
//                noteDao?.insert(Note("Title 2", "description 2"))
//                noteDao?.insert(Note("Title 3", "description 3"))
            }
        }

    }
}