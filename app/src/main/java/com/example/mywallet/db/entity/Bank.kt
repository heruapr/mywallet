package com.example.mywallet.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "banks_table")

data class Bank(
    var idBank: Int,
    var bankName: String,
    var vaCode: Int
)
{
    @PrimaryKey()
    var id: Int = idBank
}