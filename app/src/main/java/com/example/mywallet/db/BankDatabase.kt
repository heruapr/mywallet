package com.example.mywallet.db

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.mywallet.db.dao.BankDao
import com.example.mywallet.db.entity.Bank

@Database(entities = [Bank::class], version = 1)

abstract class BankDatabase : RoomDatabase() {
    abstract fun bankDao(): BankDao

    companion object {

        private var instance: BankDatabase? = null
        fun getInstance(context: Context): BankDatabase {
            if (instance == null) {
                synchronized(BankDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        BankDatabase::class.java, "bank_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build()
                }
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }

        private val roomCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                PopulateDbAsyncTask(instance)
                    .execute()
            }
        }

        class PopulateDbAsyncTask(db: BankDatabase?) : AsyncTask<Unit, Unit, Unit>() {
            private val bankDao = db?.bankDao()

            override fun doInBackground(vararg p0: Unit?) {
//                noteDao?.insert(
//                    Note(
//                        0,
//                        "Sport",
//                        "https://homepages.cae.wisc.edu/~ece533/images/peppers.png",
//                        "judul berita",
//                    "beritaaa nihhh"
//                    )
//                )
//                noteDao?.insert(Note("Title 2", "description 2"))
//                noteDao?.insert(Note("Title 3", "description 3"))
            }
        }

    }
}