package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class OTPValidationRequest(
    @SerializedName("idUser") val idUser: Int,
    @SerializedName("otp") val otp: String
) {
}