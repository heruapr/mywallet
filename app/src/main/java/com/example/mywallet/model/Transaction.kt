package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class Transaction(
    val idTransaction: String,
    val idUser: String,
    val dateTransaction: String,
    val amount: Double,
    val service: String,
    @SerializedName("is_use_wallet") val isUseWallet: Int,
    val vaNumber: String? = null,
    val isFinished: Int
) {
}