package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class CityList
    (
    @SerializedName("provinces") val cityList: List<City>
) {
}
