package com.example.mywallet.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Bill(
    @SerializedName("taxNumber") var taxNumber: String = "",
    @SerializedName("payAmount") var payAmount: Double = 0.0,
    @SerializedName("address") var address: String = "",
    @SerializedName("groundArea") var groundArea: Double = 0.0,
    @SerializedName("cityName") var cityName: String = "",
    @SerializedName("year") var year: Int = 0,
    @SerializedName("latitude") var latitude: Double = 0.0,
    @SerializedName("longitude") var longitude: Double = 0.0,
    @SerializedName("buildingArea") var buildingArea: Double = 0.0,
    @SerializedName("provinceName") var provinceName: String = ""
) : Parcelable {
}