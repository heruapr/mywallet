package com.example.mywallet.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class eWalletPaymentRequest(
    @SerializedName("taxNumber") var taxNumber: String = "",
    @SerializedName("amount") var amount: Double = 0.0,
    @SerializedName("year") var year: Int = 0,
    @SerializedName("service") var service: String = ""
)