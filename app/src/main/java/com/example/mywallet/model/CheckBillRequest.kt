package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class CheckBillRequest(
    @SerializedName("taxNumber") val taxNumber: String,
    @SerializedName("idCity") val idCity: Int,
    @SerializedName("idProvince") val idProvince: Int,
    @SerializedName("year") val year: Int
)