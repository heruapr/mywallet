package com.example.mywallet.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BankPayment(
    @SerializedName("virtualNumber") var virtualNumber: String = "", 
    @SerializedName("idTransaction") var idTransaction: Int = 0,
    @SerializedName("payAmount") var payAmount: Double = 0.0
) : Parcelable {
}