package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class BaseResponse<Any>(
    @SerializedName("status") var status: Int,
    @SerializedName("code") var code: Int,
    @SerializedName("message") var message: String,
    @SerializedName("data") var data: Any
) {
}