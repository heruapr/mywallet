package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class LogoutRequest(
    @SerializedName("idUser") val idUser: Int,
    @SerializedName("token") val token: String
) {
}