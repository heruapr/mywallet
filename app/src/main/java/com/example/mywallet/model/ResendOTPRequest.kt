package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class ResendOTPRequest(
    @SerializedName("telephone") val telephone: String
) {
}