package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class UpdateUserDataRequest(
    @SerializedName("name") val name: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("telephone") val telephone: String?,
    @SerializedName("photo") val photo: String?,
    @SerializedName("password") val password: String?,
    @SerializedName("newPassword") val newPassword: String?,
    @SerializedName("retryPassword") val retryPassword: String?
) {
}