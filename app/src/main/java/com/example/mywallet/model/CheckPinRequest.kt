package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class CheckPinRequest(
    @SerializedName("pin") val pin: Int
) {
}