package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class RegisterRequest(
    @SerializedName("name") val name: String,
    @SerializedName("email") val email: String,
    @SerializedName("telephone") val telephone: String,
    @SerializedName("password") val password: String,
    @SerializedName("retryPassword") val retryPassword: String
) {
}