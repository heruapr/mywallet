package com.example.mywallet.model

data class City(
    var idCity: Int,
    var cityName: String
)
{}