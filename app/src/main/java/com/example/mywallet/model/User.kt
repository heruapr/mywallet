package com.example.mywallet.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    @SerializedName("idUser") var idUser: Int = 0,
    @SerializedName("name") var name: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("telephone") var telephone: String? = null,
    @SerializedName("balance") var balance: Double = 0.0,
    @SerializedName("token") var token: String? = null,
    @SerializedName("pin") var pin: String? = null,
    @SerializedName("photoProfile") var photoProfile: String? = null,
    @SerializedName("isEmailActive") var isEmailActive: Int = 0,
    @SerializedName("password") var password: String? = null,
    @SerializedName("retryPassword") var retryPassword: String? = null,
    @SerializedName("newPassword") var newPassword: String? = null
) : Parcelable{
}