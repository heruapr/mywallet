package com.example.mywallet.model

import com.google.gson.annotations.SerializedName

data class UpdatePINRequest(
    @SerializedName("pin") val pin: Int,
    @SerializedName("retryPin") val retryPin: Int
) {
}