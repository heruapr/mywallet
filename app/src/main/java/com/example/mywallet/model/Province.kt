package com.example.mywallet.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Province(
    @SerializedName("idProvince") var idProvince: Int = 0,
    @SerializedName("provinceName") var provinceName: String = ""
) : Parcelable {
}